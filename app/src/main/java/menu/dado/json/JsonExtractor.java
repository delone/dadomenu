package menu.dado.json;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import menu.dado.model.MainCategory;
import menu.dado.model.Meal;
import menu.dado.model.MyOrder;
import menu.dado.model.Restaurant;
import menu.dado.model.RestaurantService;
import menu.dado.model.SubCategory;
import menu.dado.network.Constants;

/**
 * Created by User on 18.06.2016.
 */
public class JsonExtractor
{
    public Restaurant extractRestaurant(JSONObject jsonObject)
    {
        Restaurant restaurant = new Restaurant();
        try {
            JSONObject restaurantJson = jsonObject.getJSONObject("restaurant");
            JSONObject name = restaurantJson.getJSONObject("name");
            JSONObject description = restaurantJson.getJSONObject("desc");

            restaurant.setId(restaurantJson.getLong("companyid"));
            restaurant.setName("en",name.getString("en"));
            restaurant.setName("az",name.getString("az"));
            restaurant.setDescription("en",description.getString("en"));
            restaurant.setDescription("az",description.getString("az"));
            restaurant.setMenuVersion(restaurantJson.getLong("menu_version"));
            restaurant.setAddress(restaurantJson.getString("address"));
            restaurant.setPhones(restaurantJson.getString("telephone"));
            restaurant.setGpsLatitude(Double.parseDouble("1"));
            restaurant.setGpsLongitude(Double.parseDouble("2"));
            restaurant.setIconSmall(restaurantJson.getString("image_small"));
            restaurant.setIconLarge(restaurantJson.getString("image_large"));
            Log.d("EXTRACTINYO", "I AM HERE TO EXTRACT RESTAURANT FROM JSON RESPONSE " + restaurant.getMenuVersion());
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        return restaurant;
    }

    public List<RestaurantService> extractExtraServices(JSONObject jsonObject)
    {
        List<RestaurantService> restaurantServiceList = new ArrayList<>();
        try {
            Log.d("EXTRACTINYO", "I AM HERE TO EXTRACT EXTRA SERVICES FROM JSON RESPONSE");
            JSONObject extraServices = jsonObject.getJSONObject("extraservices");
            Iterator keys = extraServices.keys();
            while (keys.hasNext())
            {
                RestaurantService restaurantService = new RestaurantService();
                // Get dynamic key
                String currentDynamicKey = (String) keys.next();
                // Get dynamic key Object itself
                JSONObject currentDynamicKeyValue = extraServices.getJSONObject(currentDynamicKey);
                JSONObject name = currentDynamicKeyValue.getJSONObject("name");
                restaurantService.setId(Long.parseLong(currentDynamicKey.toString()));
                restaurantService.setName("en", name.getString("en"));
                restaurantService.setName("az", name.getString("az"));
                restaurantServiceList.add(restaurantService);
                Log.d("vavava", name.getString("en"));
                Log.d("vavava", currentDynamicKey.toString());
            }

        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        return restaurantServiceList;
    }

    public List<MainCategory> extractMainCategory(JSONObject jsonObject)
    {
        MainCategory mainCategory = null;
        List<MainCategory> mainCategoryList = new ArrayList<MainCategory>();
        try
        {
            Log.d("EXTRACTINYO", "I AM HERE TO EXTRACT MAINCATEGORY FROM JSON RESPONSE");
            JSONObject menu = jsonObject.getJSONObject("menu");
            Iterator menuKeys = menu.keys();
            while (menuKeys.hasNext()) {
                // Get dynamic key
                String currentDynamicKey = (String) menuKeys.next();
                // Get dynamic key Object itself
                JSONObject currentDynamicKeyValue = menu.getJSONObject(currentDynamicKey);

                String isCategory = currentDynamicKeyValue.getString("is_category");
                String parentId = currentDynamicKeyValue.getString("parent_id");

                // Main Category
                if (Long.parseLong(isCategory) == 1 && Long.parseLong(parentId) == 0) {
                    // Fill mainCategory
                    mainCategory = new MainCategory();

                    JSONObject name = currentDynamicKeyValue.getJSONObject("name");
                    JSONObject description = currentDynamicKeyValue.getJSONObject("desc");

                    mainCategory.setId((Long.parseLong(currentDynamicKey.toString())));
                    mainCategory.setIconSmall(currentDynamicKeyValue.getString("image_small"));
                    mainCategory.setIconLarge(currentDynamicKeyValue.getString("image_large"));
                    mainCategory.setName("en", name.getString("en"));
                    mainCategory.setName("az", name.getString("az"));
                    mainCategory.setDescription("en",description.getString("en"));
                    mainCategory.setDescription("az",description.getString("az"));

                    mainCategoryList.add(mainCategory);
                }
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        return mainCategoryList;
    }

    public List<SubCategory> extractSubCategory(JSONObject jsonObject)
    {
       SubCategory subCategory = null;
        List<SubCategory> subCategoryList = new ArrayList<SubCategory>();
        try
        {
            Log.d("EXTRACTINYO", "I AM HERE TO EXTRACT SUBCATEGORY FROM JSON RESPONSE");
            JSONObject menu = jsonObject.getJSONObject("menu");
            Iterator menuKeys = menu.keys();
            while (menuKeys.hasNext()) {
                // Get dynamic key
                String currentDynamicKey = (String) menuKeys.next();
                // Get dynamic key Object itself
                JSONObject currentDynamicKeyValue = menu.getJSONObject(currentDynamicKey);

                String isCategory = currentDynamicKeyValue.getString("is_category");
                String parentId = currentDynamicKeyValue.getString("parent_id");

                // Sub Category
                if (Long.parseLong(isCategory) == 1 && Long.parseLong(parentId) > 0)
                {
                    // Fill subCategory
                    subCategory = new SubCategory();

                    JSONObject name = currentDynamicKeyValue.getJSONObject("name");
                    JSONObject description = currentDynamicKeyValue.getJSONObject("desc");

                    subCategory.setName("en", name.getString("en"));
                    subCategory.setName("az", name.getString("az"));
                    subCategory.setDescription("en",description.getString("en"));
                    subCategory.setDescription("az",description.getString("az"));
                    subCategory.setId(Long.parseLong(currentDynamicKey.toString()));
                    subCategory.setParentId(Long.parseLong(parentId));
                    subCategory.setIconSmall(currentDynamicKeyValue.getString("image_small"));
                    subCategory.setIconLarge(currentDynamicKeyValue.getString("image_large"));

                    subCategoryList.add(subCategory);
                }
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        return subCategoryList;
    }

    public List<Meal> extractMeal(JSONObject jsonObject)
    {
        Meal meal= null;
        List<Meal> mealList = new ArrayList<Meal>();
        try
        {
            Log.d("EXTRACTINYO", "I AM HERE TO EXTRACT MEAL FROM JSON RESPONSE");
            JSONObject menu = jsonObject.getJSONObject("menu");
            Iterator menuKeys = menu.keys();
            while (menuKeys.hasNext()) {
                // Get dynamic key
                String currentDynamicKey = (String) menuKeys.next();
                // Get dynamic key Object itself
                JSONObject currentDynamicKeyValue = menu.getJSONObject(currentDynamicKey);

                String isCategory = currentDynamicKeyValue.getString("is_category");
                String parentId = currentDynamicKeyValue.getString("parent_id");

                // Meal
                if (Long.parseLong(isCategory) == 0 && Long.parseLong(parentId) > 0) {
                    // Fill meal
                    meal = new Meal();

                    JSONObject name = currentDynamicKeyValue.getJSONObject("name");
                    JSONObject description = currentDynamicKeyValue.getJSONObject("desc");

                    String price = currentDynamicKeyValue.getString("price");
                    meal.setId(Long.parseLong(currentDynamicKey.toString()));
                    meal.setName("en", name.getString("en"));
                    meal.setName("az", name.getString("az"));
                    meal.setPrice(Double.parseDouble(price));
                    meal.setDescription("en", description.getString("en"));
                    meal.setDescription("az", description.getString("az"));
                    meal.setParentId(Long.parseLong(parentId));
                    meal.setIconSmall(currentDynamicKeyValue.getString("image_small"));
                    meal.setIconLarge(currentDynamicKeyValue.getString("image_large"));
                    if(currentDynamicKeyValue.has("portions"))
                    {
                        Log.d("portionsinyo", String.valueOf(currentDynamicKeyValue.getJSONObject("portions")));
                        meal.setPortionsString(String.valueOf(currentDynamicKeyValue.getJSONObject("portions")));
//                        JSONObject portions = currentDynamicKeyValue.getJSONObject("portions");
//                        Iterator keys = portions.keys();
//                        while (keys.hasNext())
//                        {
//                            String currentKey = (String) keys.next();
//                            String currentKeyValue = portions.getString(currentKey);
//                            Log.d("portionsKeys",currentKey + " : " + currentKeyValue);
//                            meal.addPortion(currentKey, Double.parseDouble(currentKeyValue));
//                        }
                    }
                    else if(currentDynamicKeyValue.has("portions") == false)
                    {
                        meal.setPortionsString("");
                    }
                    mealList.add(meal);
                }
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        return mealList;
    }

    public MyOrder extractOrder(JSONObject jsonObject)
    {
        MyOrder order = new MyOrder();
        try {
            order.setOrderId(Long.parseLong(jsonObject.getString(Constants.OrderId)));
            Log.d("EXTRACTINYO", "I AM HERE TO EXTRACT ORDER FROM JSON ORDER " + jsonObject.getString("total_amount"));
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        return order;
    }

    public List<Restaurant> extractRestaurantList(JSONObject jsonObject)
    {
        List<Restaurant> restaurants = new ArrayList<Restaurant>();
        try
        {
            JSONObject restaurantList = jsonObject.getJSONObject("restaurants");
            Log.d("restaurantList",restaurantList.toString());
            Iterator keys = restaurantList.keys();
            while (keys.hasNext())
            {
                String currentDynamicKey = (String) keys.next();
                JSONObject currentDynamicKeyValue = restaurantList.getJSONObject(currentDynamicKey);
                JSONObject name = currentDynamicKeyValue.getJSONObject("name");
                JSONObject description = currentDynamicKeyValue.getJSONObject("desc");

                Log.d("currentDynamicKey",currentDynamicKey + "    " + currentDynamicKeyValue.toString());
                Restaurant restaurant = new Restaurant();

                restaurant.setId(Long.parseLong(currentDynamicKey));
                restaurant.setCompanyId(Integer.valueOf(currentDynamicKeyValue.getString("companyid")));
                restaurant.setDescription("en",description.getString("en"));
                restaurant.setDescription("az",description.getString("az"));
                restaurant.setAddress(currentDynamicKeyValue.getString("address"));
                restaurant.setMenuVersion(Long.valueOf(1));
                restaurant.setName("en",name.getString("en"));
                restaurant.setName("az",name.getString("az"));
                restaurant.setIconSmall(currentDynamicKeyValue.getString("image_small"));
                restaurant.setIconLarge(currentDynamicKeyValue.getString("image_large"));
                restaurant.setPhones(currentDynamicKeyValue.getString("telephone"));
                restaurant.setGpsLatitude(1);
                restaurant.setGpsLongitude(1);
                //restaurant.setGpsLatitude(Double.parseDouble(currentDynamicKeyValue.getString("coordinate_latt")));
                //restaurant.setGpsLongitude(Double.parseDouble(currentDynamicKeyValue.getString("coordinate_long")));

                restaurants.add(restaurant);
                Log.d("MENUVERSIONINYO",currentDynamicKeyValue.getString("image_small"));
                Log.d("MENUVERSIONINYO",restaurant.getIconSmall());
            }
        }
        catch (JSONException e){}
        return restaurants;
    }
}
