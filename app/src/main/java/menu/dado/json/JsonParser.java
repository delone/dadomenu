package menu.dado.json;

import android.content.Context;
import android.util.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import menu.dado.model.Restaurant;
import menu.dado.model.RestaurantMenu;

/**
 * Created by Android on 07.02.2016.
 */
public class JsonParser {
    public Restaurant parse(Context context) {
        try {
            InputStream ins = context.getResources().openRawResource(context.getResources().getIdentifier("r40", "raw", context.getPackageName()));
            return parse(context, ins);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Restaurant parse(Context context, InputStream ins) throws Exception {
        JsonReader reader = new JsonReader(new InputStreamReader(ins, "UTF-8"));
        Restaurant restaurant = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if ("error_code".equals(name)) {
                int errorCode = reader.nextInt();
                if (errorCode != 0) {
                    throw new Exception("system error");
                }
            } else if ("restaurant".equals(name)) {
                restaurant = parseRestaurant(reader) ;
            } else  {
                reader.skipValue();
            }
        }
        reader.endObject();

        return restaurant;
    }

    public Restaurant parseRestaurant(JsonReader reader) throws Exception {
        Restaurant restaurant = new Restaurant() ;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if ("companyid".equals(name)) {
               restaurant.setId(reader.nextLong());
            } else if ("menu_version".equals(name)) {
                restaurant.setMenuVersion(Long.parseLong(reader.nextString()));
            } else if ("telephone".equals(name)) {
                restaurant.setPhones(reader.nextString());
            } else if ("address".equals(name)) {
                restaurant.setAddress(reader.nextString());
//            } else if ("coordinate_latt".equals(name)) {
//                restaurant.setGpsLatitude(reader.nextDouble());
//            } else if ("coordinate_long".equals(name)) {
//                restaurant.setGpsLongitude(reader.nextDouble());
            } else if ("image_small".equals(name)) {
                restaurant.setIconSmall(reader.nextString());
            } else if ("image_large".equals(name)) {
                restaurant.setIconLarge(reader.nextString());
            } else if ("name".equals(name)) {
                restaurant.setNames(parseLocalizedText(reader)); ;
            } else if ("desc".equals(name)) {
                restaurant.setDescriptions(parseLocalizedText(reader)); ;
            } else {
                reader.skipValue(); 
            }
        }
        reader.endObject();

        return restaurant ;
    }

    public HashMap<String, String> parseLocalizedText(JsonReader reader) throws Exception {
        HashMap<String, String> map = new HashMap<String, String>() ;
        reader.beginObject();
        while (reader.hasNext()) {
            map.put(reader.nextName(), reader.nextString());
        }
        reader.endObject(); ;
        return map ;
    }

    public RestaurantMenu parseMenu(JsonReader reader) throws Exception {
        RestaurantMenu menu = new RestaurantMenu() ;
        return menu ;
    }
}
