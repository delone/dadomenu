package menu.dado.model;

/**
 * Created by User on 29.06.2016.
 */
public class Nfc
{
    private String manfId;
    private String dadoId;
    private String restId;


    public String getManfId() {
        return manfId;
    }

    public void setManfId(String manfId) {
        this.manfId = manfId;
    }

    public String getDadoId() {
        return dadoId;
    }

    public void setDadoId(String dadoId) {
        this.dadoId = dadoId;
    }

    public String getRestId() {
        return restId;
    }

    public void setRestId(String restId) {
        this.restId = restId;
    }
}
