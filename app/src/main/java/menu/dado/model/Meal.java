package menu.dado.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class Meal extends BaseMenuObject {

    private String portionsString;
    private int preparationTime ;
    private double price ;
    private String currency ;
    private ArrayList<Portion> portions = new ArrayList<Portion>();

    public int getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    public double getPrice() {
        return price;
    }

    public double getPrice(String p) {
        if ("1".equals(p)) {
            return price ;
        } else {
            for (Portion portion : portions) {
                if (p.equals(portion.getPortion())) {
                    return portion.getPrice() ;
                }
            }
            return -1 ;
        }
    }

    public void setPrice(double price) {
        this.price = price;
        portions.add(new Portion("1", price)) ;
        Collections.sort(portions, new Comparator<Portion>() {
            @Override
            public int compare(Portion lhs, Portion rhs) {
                return (int)(lhs.getPrice() * 100000) - (int)(rhs.getPrice() * 100000);
            }
        });
    }

    public ArrayList<Portion> getPortions() {
        return portions;
    }

    public void addPortion(String portion, double price) {
        portions.add(new Portion(portion, price)) ;
        Collections.sort(portions, new Comparator<Portion>() {
            @Override
            public int compare(Portion lhs, Portion rhs) {
                return (int) (lhs.getPrice() * 100000) - (int) (rhs.getPrice() * 100000);
            }
        });
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public static class Portion implements  Serializable {
        String portion ;
        double price ;

        public Portion(String portion, double price) {
            this.portion = portion ;
            this.price = price ;
        }

        public String getPortion() {
            return portion;
        }

        public double getPrice() {
            return price;
        }
    }

    public String getPortionsString() {
        return portionsString;
    }

    public void setPortionsString(String portionsString) {
        this.portionsString = portionsString;
    }
}
