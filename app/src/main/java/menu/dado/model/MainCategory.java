package menu.dado.model;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class MainCategory extends BaseMenuObject {
    private SubCategory[] items ;

    public SubCategory[] getItems() {
        return items;
    }

    public void setItems(SubCategory[] items) {
        this.items = items;
    }

    public SubCategory getSubCategory(long id) {
        for (SubCategory cat : items) {
            if (cat.getId() == id) {
                return cat ;
            }
        }
        return null ;
    }
}
