package menu.dado.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Android on 07.02.2016.
 */
public class OrderItem {
    private long mealId ;
    private Meal meal ;
    private boolean isLocked ;
    private HashMap<String, Integer> countMap = new HashMap<String, Integer>() ;

    public HashMap<String, Integer> getCountMap() {
        return countMap;
    }

    public void setCountMap(HashMap<String, Integer> countMap) {
        this.countMap = countMap;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
        this.mealId = meal.getId() ;
    }

    public long getMealId() {
        return mealId;
    }

    public void setMealId(long mealId) {
        this.mealId = mealId;
    }

    public double getPrice() {
        double totalPrice = 0 ;
        for (Map.Entry<String, Integer> count : countMap.entrySet()) {
            double price = meal.getPrice(count.getKey()) ;
            totalPrice += price * count.getValue() ;
        }

        return totalPrice ;
    }

    public int getCount() {
        int totalCount = 0 ;
        for (Map.Entry<String, Integer> count : countMap.entrySet()) {
            totalCount += count.getValue() ;
        }
        return totalCount ;
    }

    public String getPortionText() {
        StringBuilder s = new StringBuilder() ;
        String sep = "" ;
        for (Map.Entry<String, Integer> count : countMap.entrySet()) {
            s.append(sep).append(count.getKey()).append("x").append(count.getValue()) ;
            sep = "," ;
        }
        return s.toString() ;
    }

    public List<String> getPortionValue() {
        List<String> portionList = new ArrayList<String>();
        for (Map.Entry<String, Integer> count : countMap.entrySet()) {
            portionList.add(count.getKey());
        }
        return portionList;
    }

    public List<Integer> getPortionQuantity() {
        List<Integer> quantityList = new ArrayList<Integer>();
        for (Map.Entry<String, Integer> count : countMap.entrySet()) {
            quantityList.add(count.getValue());
        }
        return quantityList;
    }
}
