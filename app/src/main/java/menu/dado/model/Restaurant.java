package menu.dado.model;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class Restaurant extends BaseMenuObject {

    private Integer companyId;
    private String phones ;
    private String address ;
    private double gpsLongitude ;
    private double gpsLatitude ;
    private String iconSmall ;
    private String iconLarge ;
    private long menuVersion ;
    private RestaurantMenu menu ;
    private RestaurantService[] services ;

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = (phones == null) ? "":phones;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = (address == null)?"":address;
    }

    public double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public String getIconSmall() {
        return iconSmall;
    }

    public void setIconSmall(String iconSmall) {
        this.iconSmall = iconSmall;
    }

    public String getIconLarge() {
        return iconLarge;
    }

    public void setIconLarge(String iconLarge) {
        this.iconLarge = iconLarge;
    }

    public RestaurantMenu getMenu() {
        return menu;
    }

    public void setMenu(RestaurantMenu menu) {
        this.menu = menu;
    }

    public RestaurantService[] getServices() {
        return services;
    }

    public void setServices(RestaurantService[] services) {
        this.services = services;
    }

    public long getMenuVersion() {
        return menuVersion;
    }

    public void setMenuVersion(Long menuVersion) {
        this.menuVersion = menuVersion;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
}
