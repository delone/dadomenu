package menu.dado.model;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class SubCategory extends BaseMenuObject {
    private Meal[] items ;

    public Meal[] getItems() {
        return items;
    }

    public void setItems(Meal[] items) {
        this.items = items;
    }

    public Meal getMeal(long id) {
        for (Meal meal : items) {
            if (meal.getId() == id) {
                return meal ;
            }
        }
        return null ;
    }
}
