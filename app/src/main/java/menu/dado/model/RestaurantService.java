package menu.dado.model;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class RestaurantService extends BaseObject {
    private boolean isLocal ;
    private String localText ;

    public boolean isLocal() {
        return isLocal;
    }

    public void setIsLocal(boolean isLocal) {
        this.isLocal = isLocal;
    }

    public String getLocalText() {
        return localText;
    }

    public void setLocalText(String localText) {
        this.localText = localText;
    }
}
