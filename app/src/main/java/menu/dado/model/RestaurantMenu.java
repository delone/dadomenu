package menu.dado.model;

import java.util.ArrayList;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class RestaurantMenu {
    private Long version ;
    private MainCategory[] categories ;
    private ArrayList<Meal> allMeals ;

    public long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public MainCategory[] getCategories() {
        return categories;
    }

    public void setCategories(MainCategory[] categories) {
        this.categories = categories;
        this.allMeals = populateAllMeals(categories);
    }

    public MainCategory getMainCategory(long id) {
        for (MainCategory cat : categories) {
            if (cat.getId() == id) {
                return cat ;
            }
        }
        return null ;
    }

    public ArrayList<Meal> populateAllMeals(MainCategory[] mainCategories) {
        ArrayList<Meal> all = new ArrayList<Meal>() ;
        if(mainCategories != null)
        {
            for (MainCategory mainCat : mainCategories) {
               if(mainCat.getItems() != null)
               {
                   for (SubCategory subCat : mainCat.getItems()) {
                       if(subCat.getItems() != null)
                       {
                           for (Meal meal : subCat.getItems()) {
                               all.add(meal) ;
                           }
                       }
                   }
               }
            }
        }
        return all ;
    }

    public ArrayList<Meal> getAllMeals() {
        return allMeals;
    }
}

