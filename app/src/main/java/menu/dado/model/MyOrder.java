package menu.dado.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Android on 07.02.2016.
 */
public class MyOrder {
    public static final int STATUS_INITIAL = 0 ;
    public static final int STATUS_ORDERED = 1 ;
    public static final int STATUS_PAID = 2 ;

    private long orderId = 0;
    private String orderMessage ;
    private int orderStatus ;
    private String feedbackScore;
    private String feedbackNote;

    public String getFeedbackNote() {
        return feedbackNote;
    }

    public void setFeedbackNote(String feedbackNote) {
        this.feedbackNote = feedbackNote;
    }

    public String getFeedbackScore() {
        return feedbackScore;
    }

    public void setFeedbackScore(String feedbackScore) {
        this.feedbackScore = feedbackScore;
    }

    private ArrayList<OrderItem> items = new ArrayList<OrderItem>() ;

    public void updateItem(Meal meal, HashMap<String, Integer> countMap) {
        int counts = 0 ;
        for (Map.Entry<String, Integer> entry:countMap.entrySet()) {
            counts += entry.getValue() ;
        }

        if (counts == 0) {
            removeItem(meal) ;
            return ;
        }

        int updateIndex = -1 ;
        for (int i=0; i<items.size(); i++) {
            if (items.get(i).getMealId() == meal.getId() && !items.get(i).isLocked()) {
                updateIndex = i ;
                break ;
            }
        }

        if (updateIndex > -1) {
            items.get(updateIndex).setCountMap(countMap);
            items.get(updateIndex).setMeal(meal);
        } else {
            OrderItem item = new OrderItem() ;
            item.setMeal(meal);
            item.setCountMap(countMap);
            items.add(item) ;
        }
    }

    public void removeItem(Meal meal) {
       int removeIndex = -1 ;
       for (int i=0; i<items.size(); i++) {
           if (items.get(i).getMealId() == meal.getId() && !items.get(i).isLocked()) {
               removeIndex = i ;
               break ;
           }
       }
        if (removeIndex > -1) {
            items.remove(removeIndex) ;
        }
    }

    public boolean hasItem(Meal meal) {
        int hasIndex = -1 ;
        for (int i=0; i<items.size(); i++) {
            if (items.get(i).getMealId() == meal.getId() && !items.get(i).isLocked()) {
                hasIndex = i ;
                break ;
            }
        }
        return hasIndex > -1 ;
    }

    public boolean hasNewItem() {
        for (int i=0; i<items.size(); i++) {
            if (!items.get(i).isLocked()) {
                return true ;
            }
        }
        return false ;
    }

    public OrderItem getItem(Meal meal) {
        int getIndex = -1 ;
        for (int i=0; i<items.size(); i++) {
            if (items.get(i).getMealId() == meal.getId() && !items.get(i).isLocked()) {
                getIndex = i ;
                break ;
            }
        }
        if (getIndex > -1) {
            return items.get(getIndex) ;
        }
        return null ;
    }

    public void lockItems() {
        ArrayList<OrderItem> newItems = new ArrayList<OrderItem>() ;
        for (OrderItem item : items) {
            item.setLocked(true);
            boolean found = false ;
            for (OrderItem targetItem : newItems) {
                if (targetItem.getMealId() == item.getMealId()) {
                    for (Map.Entry<String, Integer> count:item.getCountMap().entrySet()) {
                        if (targetItem.getCountMap().containsKey(count.getKey())) {
                            int oldCount = targetItem.getCountMap().get(count.getKey()) ;
                            targetItem.getCountMap().put(count.getKey(), oldCount + count.getValue()) ;
                        } else {
                            targetItem.getCountMap().put(count.getKey(), count.getValue()) ;
                        }

                    }
                    found = true ;
                }
            }
            if (!found) {
                item.setLocked(true);
                newItems.add(item) ;
            }
        }
        items = newItems ;
    }

    public double getTotalPrice() {
        double totalPrice = 0 ;
        for (int i=0; i<items.size(); i++) {
            totalPrice += items.get(i).getPrice() ;
        }
        return totalPrice ;
    }

    public int getTotalDuration() {
        int maxDuration = 0 ;
        for (int i=0; i<items.size(); i++) {
            if (items.get(i).getMeal().getPreparationTime() > maxDuration) {
                maxDuration = items.get(i).getMeal().getPreparationTime() ;
            }
        }
        return maxDuration ;
    }

    public String getOrderMessage() {
        return orderMessage;
    }

    public void setOrderMessage(String orderMessage) {
        this.orderMessage = orderMessage;
    }

    public int getItemCount() {
        return items.size() ;
    }

    public OrderItem getItem(int index) {
        return items.get(index) ;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }
}
