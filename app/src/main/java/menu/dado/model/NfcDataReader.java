package menu.dado.model;

/**
 * Created by User on 04.07.2016.
 */
public class NfcDataReader
{
    public static NfcDataReader instance;
    private String RestId;
    private String DadoId;
    private String ManfId;

    private NfcDataReader()
    {

    }

    public static synchronized NfcDataReader getInstance()
    {
        if(instance == null)
        {
            instance = new NfcDataReader();
        }
        return instance;
    }

    public void clear()
    {
        instance = null;
    }

    public String getDadoId() {
        return DadoId;
    }

    public void setDadoId(String dadoId) {
        DadoId = dadoId;
    }

    public String getRestId() {
        return RestId;
    }

    public void setRestId(String restId) {
        RestId = restId;
    }

    public String getManfId() {
        return ManfId;
    }

    public void setManfId(String manfId) {
        ManfId = manfId;
    }
}
