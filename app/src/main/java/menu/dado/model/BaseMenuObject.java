package menu.dado.model;

import java.util.HashMap;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class BaseMenuObject extends BaseObject {
    private HashMap<String, String> description = new HashMap<String, String>() ;
    private String iconSmall ;
    private String iconLarge ;
    private String iconSmallHash ;
    private String iconLargeHash ;

    public String getDescription(String locale) {
        return description.get(locale) ;
    }

    public void setDescription(String locale, String name) {
        this.description.put(locale, name) ;
    }

    public String getIconSmall() {
        return iconSmall;
    }

    public void setIconSmall(String iconSmall) {
        this.iconSmall = iconSmall;
        this.iconSmallHash = "" + Math.abs(iconSmall.hashCode()) ;
    }

    public HashMap<String, String> getDescriptions() {
        return description;
    }

    public void setDescriptions(HashMap<String, String> description) {
        this.description = description;
    }

    public String getIconLarge() {
        return iconLarge;
    }

    public void setIconLarge(String iconLarge) {
        this.iconLarge = iconLarge;
        this.iconLargeHash = "" + Math.abs(iconLarge.hashCode()) ;
    }

    public String getIconSmallHash() {
        return iconSmallHash;
    }

    public String getIconLargeHash() {
        return iconLargeHash;
    }

}
