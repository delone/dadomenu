package menu.dado.model;

import android.content.Context;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by rdavudov on 1/24/2016.
 */
public class MealCard extends Card {
    private Meal meal ;
    private int weight ;
    public MealCard(Context context, Meal meal) {
        super(context);
        this.meal = meal ;
    }

    public Meal getMeal() {
        return meal ;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
