package menu.dado.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class BaseObject implements Serializable {
    private long id ;
    private long parentId ;
    private int order ;
    private HashMap<String, String> name = new HashMap<String, String>() ;

    public String getName(String locale) {
        return name.get(locale);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String locale, String name) {
        this.name.put(locale, name) ;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setNames(HashMap<String, String> name) {
        this.name = name;
    }
    public long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
