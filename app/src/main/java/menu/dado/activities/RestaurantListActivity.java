package menu.dado.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Process;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import menu.dado.R;
import menu.dado.fragments.adapters.RestaurantListAdapter;
import menu.dado.json.JsonExtractor;
import menu.dado.model.Restaurant;
import menu.dado.network.Constants;
import menu.dado.network.CustomVolleyRequestQueue;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.LocationProvider;
import menu.dado.sqliteDatabase.DatabaseHelper;

public class RestaurantListActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper = new DatabaseHelper();
    JsonExtractor jsonExtractor = new JsonExtractor();
    TextView textView;
    RequestQueue requestQueue;
    Restaurant [] restaurants;
    SharedPreferences sharedPreferences;
    List<Restaurant> restaurantList = new ArrayList<Restaurant>();
    RecyclerView recyclerView;
    RestaurantListAdapter restaurantListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_info);

        // Call RestaurantList Api in order to get all restaurants list
        initRestaurantList();

        if(!databaseHelper.checkDataBase(RestaurantListActivity.this))
        {
            databaseHelper.createDatabaseRestaurantMenu(RestaurantListActivity.this);
        }
    }

    private void initRestaurantList()
    {
        requestQueue = CustomVolleyRequestQueue.getInstance().getRequestQueue();
        sharedPreferences = getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);

        Map<String, String> params = new HashMap<String, String>();
        // the POST parameters:
        params.put(Constants.UserId, sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        params.put(Constants.CoordLatt, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.CoordLong, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.CompId, "0");

        final ProgressDialog loading = ProgressDialog.show(this,"Loading...","Please wait...",false,false);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, Constants.GetRestaurantListUrl,new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try
                        {
                            loading.dismiss();
                            Log.d("dadada",response.toString());
                            restaurantList = jsonExtractor.extractRestaurantList(response);
                            restaurants = new Restaurant[restaurantList.size()];
                            restaurantList.toArray(restaurants);

                            recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
                            recyclerView.setHasFixedSize(true);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerView.setLayoutManager(layoutManager);

                            final RecyclerView.Adapter adapter = new RestaurantListAdapter(restaurants);

                            recyclerView.setAdapter(adapter);

                            recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener()
                            {
                                GestureDetector gestureDetector = new GestureDetector(getApplicationContext(),
                                        new GestureDetector.SimpleOnGestureListener() {

                                            @Override public boolean onSingleTapUp(MotionEvent e) {
                                                return true;
                                            }

                                        });
                                @Override
                                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                                    View child = rv.findChildViewUnder(e.getX(), e.getY());
                                    if(child != null && gestureDetector.onTouchEvent(e)) {
                                        int position = rv.getChildAdapterPosition(child);
                                        //Toast.makeText(getApplicationContext(), restaurants[position].getName("en") + position, Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(),RestaurantBasicInfoActivity.class);
                                        intent.putExtra(Constants.Name,restaurants[position].getName(Constants.En));
                                        intent.putExtra(Constants.Description,restaurants[position].getDescription(Constants.En));
                                        intent.putExtra(Constants.Phone,restaurants[position].getPhones());
                                        intent.putExtra(Constants.Address,restaurants[position].getAddress());
                                        intent.putExtra(Constants.ImageUrl,restaurants[position].getIconLarge());
                                        startActivity(intent);
                                    }

                                    return false;
                                }

                                @Override
                                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                                }

                                @Override
                                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                            loading.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        if(error instanceof NoConnectionError || error instanceof NetworkError || error instanceof TimeoutError)
                        {
                            // show alertdialog to alert appropriate message
                            AlertDialog.Builder builder = new AlertDialog.Builder(RestaurantListActivity.this);
                            builder.setMessage(getResources().getString(R.string.no_internet_message))
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int id)
                                        {
                                            dialog.dismiss();
                                            RestaurantListActivity.this.finish();
                                            Process.killProcess(Process.myPid());
                                            System.exit(0);
                                        }
                                    })
                                    .setNegativeButton(getResources().getString(R.string.alert_dialog_Retry), new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int id)
                                        {
                                            initRestaurantList();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                        loading.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "   application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(postRequest);

    }

}
