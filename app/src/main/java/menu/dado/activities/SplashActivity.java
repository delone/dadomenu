package menu.dado.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Process;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import at.markushi.ui.RectangleButton;
import dmax.dialog.SpotsDialog;
import menu.dado.R;
import menu.dado.json.JsonExtractor;
import menu.dado.network.Constants;
import menu.dado.network.CustomVolleyRequestQueue;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.LocationProvider;
import menu.dado.provider.Utility;
import menu.dado.sqliteDatabase.DatabaseConstants;
import menu.dado.sqliteDatabase.DatabaseHelper;

public class SplashActivity extends AppCompatActivity{

    DatabaseHelper databaseHelper = new DatabaseHelper();
    JsonExtractor jsonExtractor = new JsonExtractor();
    SharedPreferences sharedpreferences;
    RequestQueue requestQueue;
    private SpotsDialog dialog ;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        requestQueue = CustomVolleyRequestQueue.getInstance().getRequestQueue();
        sharedpreferences = getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);
        //TODO: get restaurant id from NFC
        int resId = 1 ;

        if (!Utility.isShowSplash(resId)) {
            Intent intent = new Intent(SplashActivity.this, RestaurantListActivity.class) ;
            startActivity(intent);
            finish();
        }
        RectangleButton button = (RectangleButton) findViewById(R.id.btn_menu) ;

        if (sharedpreferences.contains(Constants.RegisteredUserId)) {
            dialog = new SpotsDialog(this, "Opening") ;
            dialog.show();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
            Intent intent = new Intent(SplashActivity.this, RestaurantListActivity.class) ;
            startActivity(intent);
            finish();
        }

        else if(sharedpreferences.contains(Constants.RegisteredUserId) == false)
        {
            createUser();
            dialog = new SpotsDialog(this, "Registering");
            dialog.show();
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            createUser();
        }

        if (resId == -1) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SplashActivity.this, HowToActivity.class) ;
                    startActivity(intent);
                    finish(); ;
                }
            });
        } else {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Intent intent = new Intent(SplashActivity.this, RestaurantListActivity.class) ;
                    startActivity(intent);
                    finish(); ;
                }
            });
        }
    }


    public void createUser()
    {
        Map<String, String> params = new HashMap<String, String>();
        // the POST parameters:
        params.put(Constants.UserId, "0");
        params.put(Constants.CoordLatt, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.CoordLong, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.ImsiId, "");
        params.put(Constants.ImeiId, "");

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, Constants.UserCreateUrl,new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try
                        {
                            sharedpreferences = getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(Constants.RegisteredUserId,response.getString("user_id"));
                            Log.d("putinyo",response.toString());
                            editor.commit();
                            dialog.dismiss();
                                Intent intent = new Intent(SplashActivity.this, RestaurantListActivity.class) ;
                                startActivity(intent);
                                finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof NoConnectionError || error instanceof NetworkError || error instanceof TimeoutError)
                        {
                            AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                            alertDialog.setTitle("Dado Menu");
                            alertDialog.setMessage(Constants.AlertDialogNetworkMessage);
                            // alertDialog.setIcon(R.drawable.icon);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            SplashActivity.this.finish();
                                            Process.killProcess(Process.myPid());
                                            System.exit(0);
                                        }
                                    });
                            alertDialog.show();
                        }
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "   application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(postRequest);
    }
}
