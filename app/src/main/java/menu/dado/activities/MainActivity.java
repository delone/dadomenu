package menu.dado.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.os.Process;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.jeremyfeinstein.slidingmenu.lib.OnScrollingListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.markushi.ui.SolidCircleButton;
import menu.dado.R;
import menu.dado.fragments.MainMenuFragment;
import menu.dado.fragments.MyOrderFragment;
import menu.dado.fragments.SearchFragment;
import menu.dado.fragments.SubMenuFragment;
import menu.dado.fragments.SubMenuListFragment;
import menu.dado.fragments.extra.ExtraServiceDialog;
import menu.dado.json.JsonExtractor;
import menu.dado.model.ApiResult;
import menu.dado.model.MainCategory;
import menu.dado.model.MyOrder;
import menu.dado.model.NfcDataReader;
import menu.dado.model.Restaurant;
import menu.dado.model.RestaurantMenu;
import menu.dado.network.Constants;
import menu.dado.network.CustomVolleyRequestQueue;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.LocationProvider;
import menu.dado.provider.MainActivityProvider;
import menu.dado.provider.RestaurantProvider;
import menu.dado.sqliteDatabase.DatabaseConstants;
import menu.dado.sqliteDatabase.DatabaseHelper;

public class MainActivity extends SlidingFragmentActivity{

    ProgressDialog loading;
    private SlidingMenu slidingMenu ;
    private MyOrderFragment myOrderFragment ;
    private SolidCircleButton btnFabMyOrder;
    private SolidCircleButton btnFabSubMenuList;
    private MainMenuFragment mainMenuFragment ;
    private SubMenuListFragment subMenuListFragment ;
    private SearchFragment searchFragment ;
    private int screenWidth;
    private float actionBarElevation ;
    private boolean isBehidViewActive ;
    private boolean isSubMenuListShown ;
    private SharedPreferences sharedPreferences;
    private RequestQueue requestQueue;
    private DatabaseHelper databaseHelper;
    public static final String MIME_TEXT_PLAIN = "text/plain";
    public static final String TAG = "NfcDemo";
    InsertMenuTask insertMenuTask;
    private NfcAdapter mNfcAdapter;
    NfcDataReader nfcDataReader;
    AsyncTask deleteTask;
    AsyncTask insertTask;

    Context context;
    String manfId;
    String dadoId;
    String restId;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivityProvider mainActivityProvider = MainActivityProvider.getInstance();
        mainActivityProvider.setActivity(MainActivity.this);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
        setBehindContentView(R.layout.activity_my_order);
        initFabButtons();
        initActionBar();
        initSlidingMenu();

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        Intent intent = getIntent();
        if(intent.getStringExtra("language") != null && intent.getStringExtra("language").equals("1"))
        {
            initMainMenuFragment();
            initMyOrderFragment();
            initSubMenuListFragment();
            getSupportFragmentManager().addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        public void onBackStackChanged() {
                            int backCount = getSupportFragmentManager().getBackStackEntryCount();
                            if (backCount == 0) {
                                setTitle(getResources().getString(R.string.app_name));
                                showSubMenuListFragment(false, true);
                            }
                        }
                    });
        }
        else
        {
            handleIntent(getIntent());
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setupForegroundDispatch(this, mNfcAdapter);
    }

    @Override
    protected void onPause()
    {
        stopForegroundDispatch(this, mNfcAdapter);
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        handleIntent(intent);
    }

    public void setupForegroundDispatch(final Activity activity, NfcAdapter adapter)
    {
        final Intent intent = new Intent(activity.getApplicationContext(), MainActivity.class);
//        final Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    private void handleIntent(Intent intent)
    {
        //TODO:handle intent

        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NdefReaderTask().execute(tag);

            } else {
                Log.d(TAG, "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new NdefReaderTask().execute(tag);
                    break;
                }
            }
        }
    }

    private class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];

            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                return null;
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG, "Unsupported Encoding", e);
                    }
                }
            }

            return null;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */

            byte[] payload = record.getPayload();

            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

            // Get the Language Code
            int languageCodeLength = payload[0] & 0063;

            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"

            // Get the Text
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null)
            {
                try
                {
                    String[] parts = result.split("#");
                    String restId = parts[0]; // 004
                    String dadoId = parts[1]; // 034556
                    String manfId = parts[2]; // 034556
                    nfcDataReader = null;
                    nfcDataReader = NfcDataReader.getInstance();
                    nfcDataReader.setRestId(restId);
                    nfcDataReader.setDadoId(dadoId);
                    nfcDataReader.setManfId(manfId);

                    Log.d("dadada",nfcDataReader.getDadoId() + " " + nfcDataReader.getRestId() + " " + nfcDataReader.getManfId());
                    if(nfcDataReader.getRestId() != null)
                    {
                        databaseHelper = new DatabaseHelper();
                        getRestaurantInfo(nfcDataReader.getRestId());
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this,getResources().getString(R.string.tap_nfc_again_message),Toast.LENGTH_LONG).show();
                    }
                }
                catch (IndexOutOfBoundsException e)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.not_appropriate_nfc_tag_message))
                            .setCancelable(false)
                            .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    dialog.dismiss();
                                    Intent intent = new Intent(MainActivity.this, RestaurantListActivity.class) ;
                                    startActivity(intent);
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        }
    }

    private void initFabButtons() {
        btnFabMyOrder = (SolidCircleButton) findViewById(R.id.btn_my_order);
        btnFabMyOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                slidingMenu.showMenu(true);
            }
        });

        btnFabSubMenuList = (SolidCircleButton) findViewById(R.id.btn_sub_menu_list);
        btnFabSubMenuList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                slidingMenu.showSecondaryMenu(true);
            }
        });
        btnFabSubMenuList.setVisibility(View.INVISIBLE);
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBarElevation = actionBar.getElevation() ;
    }

    private String actionBarTitle ;

    public void initSlidingMenu() {
        slidingMenu = getSlidingMenu();
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        slidingMenu.setShadowDrawable(R.drawable.sliding_menu_shadow_left);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.SLIDING_WINDOW);
        slidingMenu.setSecondaryOnOpenListner(new SlidingMenu.OnOpenListener() {
            public void onOpen() {
                isBehidViewActive = true;
            }
        });
        slidingMenu.setOnCloseListener(new SlidingMenu.OnCloseListener() {
            public void onClose() {
                if (myOrderFragment != null) {
                    btnFabMyOrder.setRotation(0);
                    invalidateOptionsMenu();
                }
                setTitle(actionBarTitle);
                isBehidViewActive = false;
                getSupportActionBar().setElevation(0);
            }
        });
        slidingMenu.setOnOpenListener(new SlidingMenu.OnOpenListener() {
            public void onOpen() {
                if (myOrderFragment != null) {
//                    currentTitle = getTitle().toString() ;
//                    if (ordersFragment.getOrder().getId() > Order.TEMPORARY_ORDER_ID) {
//                        setTitle(R.string.order_title) ;
//                        setTitle(getTitle() + " #" + ordersFragment.getOrder().getId()) ;
//                    } else {
                    actionBarTitle = getTitle().toString() ;
                    setTitle(R.string.order_title);
//                    }
                    btnFabMyOrder.setRotation(180);
                    invalidateOptionsMenu();
                }
                isBehidViewActive = true;
                getSupportActionBar().setElevation(actionBarElevation);
            }
        });

        DisplayMetrics dimension = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dimension);
        int offset = (int) getResources().getDimension(R.dimen.slidingmenu_offset);

        screenWidth = dimension.widthPixels - offset;

        slidingMenu.setScrollingListener(new OnScrollingListener() {
            public void onScroll(int scrollX) {
                int percent = Math.abs(scrollX) * 100 / screenWidth;
                int rotation = 180 * percent / 100;
                float elevation = actionBarElevation * percent / 100 ;
                btnFabMyOrder.setRotation(rotation);
                getSupportActionBar().setElevation(elevation);
            }
        });

        setSlidingActionBarEnabled(false);
    }

    public void initSlidingMenuForFragment() {
        slidingMenu = getSlidingMenu();
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        slidingMenu.setShadowDrawable(R.drawable.sliding_menu_shadow_left);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.SLIDING_WINDOW);
        slidingMenu.setSecondaryOnOpenListner(new SlidingMenu.OnOpenListener() {
            public void onOpen() {
                isBehidViewActive = true;
            }
        });
        slidingMenu.setOnCloseListener(new SlidingMenu.OnCloseListener() {
            public void onClose() {
                if (myOrderFragment != null) {
                    btnFabMyOrder.setRotation(0);
                    invalidateOptionsMenu();
                }
                setTitle(actionBarTitle);
                isBehidViewActive = false;
                getSupportActionBar().setElevation(0);
            }
        });
        slidingMenu.setOnOpenListener(new SlidingMenu.OnOpenListener() {
            public void onOpen() {
                if (myOrderFragment != null) {
//                    currentTitle = getTitle().toString() ;
//                    if (ordersFragment.getOrder().getId() > Order.TEMPORARY_ORDER_ID) {
//                        setTitle(R.string.order_title) ;
//                        setTitle(getTitle() + " #" + ordersFragment.getOrder().getId()) ;
//                    } else {
                    actionBarTitle = getTitle().toString() ;
                    setTitle(R.string.order_title);
//                    }
                    btnFabMyOrder.setRotation(180);
                    invalidateOptionsMenu();
                }
                isBehidViewActive = true;
                getSupportActionBar().setElevation(actionBarElevation);
            }
        });

        DisplayMetrics dimension = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dimension);
        int offset = (int) getResources().getDimension(R.dimen.slidingmenu_offset);

        screenWidth = dimension.widthPixels - offset;

        slidingMenu.setScrollingListener(new OnScrollingListener() {
            public void onScroll(int scrollX) {
                int percent = Math.abs(scrollX) * 100 / screenWidth;
                int rotation = 180 * percent / 100;
                float elevation = actionBarElevation * percent / 100 ;
                btnFabMyOrder.setRotation(rotation);
                getSupportActionBar().setElevation(elevation);
            }
        });

         //setSlidingActionBarEnabled(false);
        slidingMenu.showMenu();
    }

    private void initMyOrderFragment() {
        myOrderFragment = new MyOrderFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.order_frame, myOrderFragment, "MYORDER")
                .commit();
    }

    private void initSubMenuListFragment() {
        slidingMenu.setSecondaryMenu(R.layout.activity_sub_menu_list);
        slidingMenu.setSecondaryShadowDrawable(R.drawable.sliding_menu_shadow_right);
        subMenuListFragment = new SubMenuListFragment() ;
        subMenuListFragment.setMainActivity(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.sub_menu, subMenuListFragment)
                .commit();
    }

    private void initMainMenuFragment() {
        if (mainMenuFragment == null) {
            mainMenuFragment = new MainMenuFragment();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, mainMenuFragment)
                .commit();
    }

    public void initSubMenuFragment(long mainCategoryId) {
        MainCategory mainCategory = new RestaurantProvider().getRestaurant(this).getMenu().getMainCategory(mainCategoryId) ;
        RestaurantProvider provider = new RestaurantProvider();
        // mainCategory.getItems().length == 1
        if(mainCategory.getItems().length > 0)
        {
//            SubMenuFragmentSingle subMenuFragment = new SubMenuFragmentSingle();
//            subMenuFragment.setMainCategoryId(mainCategoryId);
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.content_frame, subMenuFragment)
//                    .addToBackStack("main")
//                    .commit();
//
//            showSubMenuListFragment(false, true);
            SubMenuFragment subMenuFragment = new SubMenuFragment();
            subMenuFragment.setMainCategoryId(mainCategoryId);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, subMenuFragment)
                    .addToBackStack("main")
                    .commit();
            showSubMenuListFragment(true, true);
            subMenuListFragment.setSubMenu(mainCategory.getItems()) ;

            subMenuListFragment.setSubMenuFragment(subMenuFragment);
        } else {
//            SubMenuFragment subMenuFragment = new SubMenuFragment();
//            subMenuFragment.setMainCategoryId(mainCategoryId);
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.content_frame, subMenuFragment)
//                    .addToBackStack("main")
//                    .commit();
//            showSubMenuListFragment(true, true);
//            subMenuListFragment.setSubMenu(mainCategory.getItems()) ;
//            subMenuListFragment.setSubMenuFragment(subMenuFragment);

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(getResources().getString(R.string.no_subcategory_message))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }
        setTitle(mainCategory.getName(getResources().getString(R.string.locale)));
    }

    public void initSearchFragment(boolean isShown) {
            searchFragment = new SearchFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, searchFragment)
                    .addToBackStack("main")
                    .commit();
            showSubMenuListFragment(false, false);
    }

    public void showSubMenuListFragment(boolean isShow, boolean keepState) {
        if (keepState)
            isSubMenuListShown = isShow ;
        if (isShow) {
            slidingMenu.setMode(SlidingMenu.LEFT_RIGHT);
            btnFabSubMenuList.setVisibility(View.VISIBLE);
        } else {
            slidingMenu.setMode(SlidingMenu.LEFT);
            btnFabSubMenuList.setVisibility(View.INVISIBLE);
        }
    }

    public void closeRightSideBar() {
        slidingMenu.toggle(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                int backCount = getSupportFragmentManager().getBackStackEntryCount();
                if (backCount == 0) {
                    if (isBehidViewActive) {
                        slidingMenu.showContent(true);
                    } else {
                        slidingMenu.showMenu(true);
                    }
                } else {
                    getSupportFragmentManager().popBackStack();
                }
                break ;
            case R.id.menu_help:
                new ExtraServiceDialog(this).createDialog().show();
                break ;
            case R.id.menu_refresh:
                myOrderFragment.refreshOrder() ;
                break ;
            case R.id.menu_language:
                showChangeLangDialog();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (isBehidViewActive) {
            getMenuInflater().inflate(R.menu.menu_myorder, menu);
//            MenuItem helpItem = menu.findItem(R.id.menu_help) ;
            MenuItem menuItem = menu.findItem(R.id.menu_refresh) ;
            if (myOrderFragment.isLoading()) {
                menuItem.setActionView(R.layout.indeterminate_progress_action);
//                menuItem.setVisible(false) ;
            } else {
                menuItem.setActionView(null) ;
//                menuItem.setVisible(true) ;
            }
        } else {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            final MenuItem menuItem = menu.findItem(R.id.menu_search) ;
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem) ;

            MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    initSearchFragment(true);
                    return true ;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    searchFragment = null ;
                    getSupportFragmentManager().popBackStackImmediate() ;
                    showSubMenuListFragment(isSubMenuListShown, false);
                    return true ;
                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                public boolean onQueryTextSubmit(String query) {
                    if (menuItem.isActionViewExpanded() && searchFragment != null) {
                        searchFragment.updateSearch(query);
                    }
                    return true;
                }
                public boolean onQueryTextChange(String newText) {
                    if (menuItem.isActionViewExpanded() && searchFragment != null) {
                        searchFragment.updateSearch(newText);
                    }
                    return true;
                }
            }) ;

            searchView.setFocusableInTouchMode(true);
            try {
                Field searchField = SearchView.class.getDeclaredField("mSearchPlate");
                searchField.setAccessible(true);
                LinearLayout searchPlate = (LinearLayout)searchField.get(searchView);

                int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
                TextView textView = (TextView) searchView.findViewById(id);
                //textView.setTextColor(Color.WHITE);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (searchFragment != null) {
                menuItem.expandActionView() ;
                searchView.post(new Runnable() {
                    public void run() {
                        searchView.setQuery(searchFragment.getLastSearch(), true);
                    }
                });
            }
        }
        return true;
    }

    private boolean filterNfcTag (String data)
    {
        Pattern pattern =  Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(data);
        if (matcher.matches()){
           return true;
        }
        else
        {
            return false;
        }
    }

    private boolean filterNfcTagId (String data)
    {
        Pattern pattern =  Pattern.compile("^[a-zA-Z0-9]*$");
        Matcher matcher = pattern.matcher(data);
        if (matcher.matches()){
            return true;
        }
        else
        {
            return false;
        }
    }

    public void showChangeLangDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.language_dialog, null);
        dialogBuilder.setView(dialogView);

        final Spinner spinner1 = (Spinner) dialogView.findViewById(R.id.spinner1);

        dialogBuilder.setTitle(getResources().getString(R.string.lang_dialog_title));
        dialogBuilder.setMessage(getResources().getString(R.string.lang_dialog_message));
        dialogBuilder.setPositiveButton(getResources().getString(R.string.change_language_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int langpos = spinner1.getSelectedItemPosition();
                switch(langpos) {
                    case 0: //English
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("LANG", "en").commit();
                        setLangRecreate("en");
                        return;
                    case 1: //Hindi
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("LANG", "az").commit();
                        setLangRecreate("az");
                        return;
                    default: //By default set to english
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("LANG", "en").commit();
                        setLangRecreate("en");
                        return;
                }
            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_language_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        restartActivity();
    }

    private void restartActivity()
    {
        Intent intent = new Intent(this, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("language","1");
        startActivity(intent);
        finish();
    }

    public void getRestaurantInfo(String restId)
    {
        //TODO: Rest id has been read from Nfc and has been written to NfcdataReader instance fields
        sharedPreferences = getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);
        Log.d("useridinyo",sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        Map<String, String> params = new HashMap<String, String>();
        // the POST parameters:
        params.put(Constants.UserId,sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        params.put(Constants.CoordLatt, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.CoordLong, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.RestId, restId);
        final ProgressDialog loading = ProgressDialog.show(this,"Loading...","Please wait...",false,false);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, Constants.GetRestaurantInfoUrl,new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try
                        {
                            if(response.getString("error_code").equals("0"))
                            {
                                Restaurant restaurant1 = databaseHelper.selectRestaurant(AppContextProvider.getContext(),nfcDataReader.getRestId());
                               if(restaurant1.getMenuVersion()>0)
                               {
                                   String menuVersion;
                                   JSONObject restaurant = response.getJSONObject("restaurant");
                                   menuVersion = (restaurant.isNull("menu_version")==true)?"000000987600":restaurant.getString("menu_version");
                                   response.put("menu_version",menuVersion);
                                   Log.d("RESPONSINYO","I AM HERE TO CHECK WHETHER MENU VERSION HAS CHANGED OR NOT");
                                   if(databaseHelper.selectRestaurant(MainActivity.this,nfcDataReader.getRestId()).getMenuVersion() == Long.parseLong(menuVersion))
                                   {
                                       loading.dismiss();
                                       Log.d("CHECKINGYO","NO CHANGES WERE DETECTED IN MENU VERSION");
                                       initMainMenuFragment();
                                       initMyOrderFragment();
                                       initSubMenuListFragment();
                                       getSupportFragmentManager().addOnBackStackChangedListener(
                                               new FragmentManager.OnBackStackChangedListener() {
                                                   public void onBackStackChanged() {
                                                       int backCount = getSupportFragmentManager().getBackStackEntryCount();
                                                       if (backCount == 0) {
                                                           setTitle(getResources().getString(R.string.app_name));
                                                           showSubMenuListFragment(false, true);
                                                       }
                                                   }
                                               });
                                   }
                                   else
                                   {
                                       Log.d("CHECKINGYO","A CHANGE WAS DETECTED IN MENU VERSION SO I AM GONNA CALL API OF RESTAURANT_INFO");
                                       //TODO: / Use AsyncTask
                                       insertTask = new DeleteandInsertMenuTask().execute(response) ;
                                       loading.dismiss();
                                   }

                                   loading.dismiss();
                               }
                                else
                               {
                                   insertTask = new InsertMenuTask().execute(response) ;
                                   loading.dismiss();
                               }
                            }

                            else
                            {
                                // dismiss loading dialog first
                                loading.dismiss();
                                // show alertdialog to alert appropriate message
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage(getResources().getString(R.string.no_such_restaurant_message))
                                        .setCancelable(false)
                                        .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int id)
                                            {
                                                dialog.dismiss();
                                                Intent intent = new Intent(MainActivity.this, RestaurantListActivity.class) ;
                                                startActivity(intent);
                                                finish();
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        loading.dismiss();
                        if(error instanceof NoConnectionError || error instanceof NetworkError || error instanceof TimeoutError)
                        {
                            // dismiss loading dilog first


                            // show alertdialog to alert appropriate message
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setMessage(getResources().getString(R.string.no_internet_message))
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int id)
                                         {
                                            dialog.dismiss();
                                            Restaurant restaurant = databaseHelper.selectRestaurant(AppContextProvider.getContext(),nfcDataReader.getRestId());
                                            if(restaurant.getMenuVersion() >0)
                                            {
                                                initMainMenuFragment();
                                                initMyOrderFragment();
                                                initSubMenuListFragment();
                                                getSupportFragmentManager().addOnBackStackChangedListener(
                                                        new FragmentManager.OnBackStackChangedListener() {
                                                            public void onBackStackChanged() {
                                                                int backCount = getSupportFragmentManager().getBackStackEntryCount();
                                                                if (backCount == 0) {
                                                                    setTitle(getResources().getString(R.string.app_name));
                                                                    showSubMenuListFragment(false, true);
                                                                }
                                                            }
                                                        });
                                            }
                                            else
                                            {
                                                finish();
                                                Process.killProcess(Process.myPid());
                                                System.exit(0);
                                            }
                                        }
                                    })
                                    .setNegativeButton(getResources().getString(R.string.alert_dialog_Retry), new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int id)
                                        {
                                            getRestaurantInfo(nfcDataReader.getRestId());
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "   application/json; charset=utf-8");
                return headers;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(4 * 1000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue = CustomVolleyRequestQueue.getInstance().getRequestQueue();
        requestQueue.add(postRequest);
    }

    public class DeleteandInsertMenuTask extends AsyncTask<JSONObject, Void, String>
    {
        protected String doInBackground(JSONObject  ... params)
        {
            databaseHelper.deleteRestaurant(AppContextProvider.getContext(),nfcDataReader.getRestId());
            databaseHelper.deleteExtraServices(AppContextProvider.getContext(),nfcDataReader.getRestId());
            databaseHelper.deleteMainCategory(AppContextProvider.getContext(),nfcDataReader.getRestId());
            databaseHelper.deleteSubCategory(AppContextProvider.getContext(),nfcDataReader.getRestId());
            databaseHelper.deleteMeal(AppContextProvider.getContext(),nfcDataReader.getRestId());

            JSONObject response = params[0];
            JsonExtractor jsonExtractor = new JsonExtractor();
            databaseHelper.insertRestaurant(AppContextProvider.getContext(),jsonExtractor.extractRestaurant(response));
            databaseHelper.insertExtraServices(AppContextProvider.getContext(),jsonExtractor.extractExtraServices(response));
            databaseHelper.insertMainCategory(AppContextProvider.getContext(),jsonExtractor.extractMainCategory(response));
            databaseHelper.insertSubCategory(AppContextProvider.getContext(),jsonExtractor.extractSubCategory(response));
            databaseHelper.insertMeal(AppContextProvider.getContext(),jsonExtractor.extractMeal(response));
            return "";
        }

        @Override
        protected void onPostExecute(String result)
        {
            initMainMenuFragment();
            initMyOrderFragment();
            initSubMenuListFragment();
            getSupportFragmentManager().addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        public void onBackStackChanged() {
                            int backCount = getSupportFragmentManager().getBackStackEntryCount();
                            if (backCount == 0) {
                                setTitle(getResources().getString(R.string.app_name));
                                showSubMenuListFragment(false, true);
                            }
                        }
                    });
        }

        @Override
        protected void onCancelled()
        {
        }
    }

    public class InsertMenuTask extends AsyncTask<JSONObject, Void, String>
    {
        protected String doInBackground(JSONObject  ... params)
        {
            JSONObject response = params[0];
            JsonExtractor jsonExtractor = new JsonExtractor();
            databaseHelper.insertRestaurant(AppContextProvider.getContext(),jsonExtractor.extractRestaurant(response));
            databaseHelper.insertExtraServices(AppContextProvider.getContext(),jsonExtractor.extractExtraServices(response));
            databaseHelper.insertMainCategory(AppContextProvider.getContext(),jsonExtractor.extractMainCategory(response));
            databaseHelper.insertSubCategory(AppContextProvider.getContext(),jsonExtractor.extractSubCategory(response));
            databaseHelper.insertMeal(AppContextProvider.getContext(),jsonExtractor.extractMeal(response));
            return "";
        }

        @Override
        protected void onPostExecute(String result)
        {
            initMainMenuFragment();
            initMyOrderFragment();
            initSubMenuListFragment();
            getSupportFragmentManager().addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        public void onBackStackChanged() {
                            int backCount = getSupportFragmentManager().getBackStackEntryCount();
                            if (backCount == 0) {
                                setTitle(getResources().getString(R.string.app_name));
                                showSubMenuListFragment(false, true);
                            }
                        }
                    });
        }

        @Override
        protected void onCancelled()
        {
        }
    }
}