package menu.dado.activities;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import menu.dado.R;
import menu.dado.model.NfcDataReader;
import menu.dado.network.Constants;
import menu.dado.provider.RestaurantProvider;

public class NfcReaderActivity extends AppCompatActivity {

    NfcAdapter nfcAdapter;
    String manfId;
    String dadoId;
    String restId;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfe_reader);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            Toast.makeText(this, "Your device does not support NFC technology", Toast.LENGTH_LONG).show();
        }
        if (nfcAdapter.isEnabled() == false) {
            Toast.makeText(this, "Please enable Nfc", Toast.LENGTH_LONG).show();
        }

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(Constants.AlertDialogTitle);
        alertDialog.setMessage(Constants.AlertDialogWelcomeMessage);
        //alertDialog.setIcon(R.drawable.icon);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableForegroundDispatchSystem();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableForegroundDispatchSystem();
    }

    protected void onNewIntent(Intent intent) {
        Log.d("afafafaf","I am on new intent");
        super.onNewIntent(intent);
        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Toast.makeText(this, "Nfc intent was received", Toast.LENGTH_LONG).show();
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            byte[] tagSerialId = tag.getId();
            manfId = new String();
            for (int i = 0; i < tagSerialId.length; i++) {
                String x = Integer.toHexString(((int) tagSerialId[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                manfId += x;
            }
            Log.i("EHEHEHEHEHE", manfId);

            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (parcelables != null && parcelables.length > 0) {
                // read and display
                dadoId = readTextFromMessage((NdefMessage) parcelables[0]);
                restId = readTextFromMessage((NdefMessage) parcelables[0]);
            } else {
                Toast.makeText(this, "Tag is empty", Toast.LENGTH_LONG).show();
                dadoId = "";
                restId = "";
            }

            NfcDataReader nfcDataReader = NfcDataReader.getInstance();
            nfcDataReader.setManfId(manfId);
            nfcDataReader.setDadoId(dadoId);
            nfcDataReader.setRestId(restId);

            //TODO: first i was thinking that it was good to write to sharedprefences.then i realizde that no singleton object is better
            //Todo : because when app is finisihed singleton objects data is removed.
//            sharedPreferences = getSharedPreferences(Constants.ManfIdSavedFileName, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putString(Constants.ManfIdKey,manfId);
//
//            sharedPreferences = getSharedPreferences(Constants.DadoIdSavedFileName, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor1 = sharedPreferences.edit();
//            editor1.putString(Constants.DadoIdKey,dadoId);

            Log.d("papapapa",nfcDataReader.getDadoId());
            Log.d("papapapa",nfcDataReader.getManfId());
            Log.d("papapapa",nfcDataReader.getRestId());
            //TODO: check manfid , restid and  dadoid..if they are appropriate then pass to mainActivity .otherwise not
            if(nfcDataReader.getDadoId().length()!=0 && nfcDataReader.getManfId().length() != 0 && nfcDataReader.getRestId().length() != 0)
            {
                new RestaurantProvider().emptyOrder();
                intent = new Intent(this,MainActivity.class);
                startActivity(intent);
            }
        }
    }

    private void enableForegroundDispatchSystem() {
        Intent intent = new Intent(this, NfcReaderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        IntentFilter[] intentFilters = new IntentFilter[]{};
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);

    }

    private void disableForegroundDispatchSystem() {
        nfcAdapter.disableForegroundDispatch(this);
    }

    // Tag Readers
    public String getTextFromNdefRecord(NdefRecord ndefRecord) {
        String tagContent = null;
        try {
            byte[] payload = ndefRecord.getPayload();
            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
            // Get the Language Code
            int languageSize = payload[0] & 0063;
            // Get the Text
            tagContent = new String(payload, languageSize + 1, payload.length - languageSize - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("getTextFromNdefRecord", e.getMessage(), e);
        }
        return tagContent;
    }

    private String readTextFromMessage(NdefMessage ndefMessage) {
        NdefRecord[] ndefRecords = ndefMessage.getRecords();
        String tagContent = "";
        if (ndefRecords != null && ndefRecords.length > 0) {
            NdefRecord ndefRecord = ndefRecords[0];
            tagContent = getTextFromNdefRecord(ndefRecord);
            Log.d("NFCRECORD", tagContent);
        } else {
            Toast.makeText(this, "No NDEF records found!", Toast.LENGTH_SHORT).show();
        }
        return tagContent;
    }
}
