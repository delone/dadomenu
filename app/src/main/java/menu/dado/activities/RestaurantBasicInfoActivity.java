package menu.dado.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import menu.dado.R;
import menu.dado.network.Constants;
import menu.dado.network.CustomVolleyRequestQueue;

public class RestaurantBasicInfoActivity extends AppCompatActivity {

    private TextView restaurantName;
    private TextView restaurantDescription;
    private TextView restaurantAddress;
    private TextView restaurantPhones;
    private NetworkImageView restaurantImage;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_basic_info);
        initActionBar();
        Intent intent = getIntent();
        restaurantName = (TextView) findViewById(R.id.restaurant_basic_info_activity_restaurant_name);
        restaurantDescription = (TextView) findViewById(R.id.restaurant_basic_info_activity_restaurant_description);
        restaurantAddress = (TextView) findViewById(R.id.restaurant_basic_info_activity_restaurant_address);
        restaurantPhones = (TextView) findViewById(R.id.restaurant_basic_info_activity_restaurant_phone);

        restaurantImage = (NetworkImageView) findViewById(R.id.restaurant_details_image);
        restaurantName.setText((intent.getStringExtra(Constants.Name).length()<=0)?Constants.RestaurantDefaultName:intent.getStringExtra(Constants.Name));
        restaurantDescription.setText((intent.getStringExtra(Constants.Description).length()<=0)?Constants.RestaurantDefaultDescription:intent.getStringExtra(Constants.Description));
        restaurantAddress.setText((intent.getStringExtra(Constants.Address).length()<=0)?Constants.RestaurantDefaultAddress:intent.getStringExtra(Constants.Address));
        restaurantPhones.setText((intent.getStringExtra(Constants.Phone).length()<=0)?Constants.RestaurantDefaultPhone:intent.getStringExtra(Constants.Phone));

        String imgUrl = (intent.getStringExtra(Constants.ImageUrl).length()<=0)?Constants.RestaurantDefaultImageUrl:intent.getStringExtra(Constants.ImageUrl); // URL of the image

        ImageLoader imageLoader = CustomVolleyRequestQueue.getImageLoader();
        imageLoader.get(imgUrl,ImageLoader.getImageListener(restaurantImage,R.drawable.pizza,R.drawable.pizza));
        restaurantImage.setImageUrl(imgUrl,imageLoader);

    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
    }

}
