package menu.dado.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import menu.dado.R;
import menu.dado.fragments.extra.MealAddDialog;
import menu.dado.model.Meal;
import menu.dado.provider.RestaurantProvider;

public class DetailActivity extends AppCompatActivity {

    private Meal meal ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        meal = (Meal) getIntent().getSerializableExtra("meal") ;
        getSupportActionBar().setTitle(meal.getName("en"));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MealAddDialog(DetailActivity.this, meal).createDialog().show();
            }
        });

        TextView description = (TextView) findViewById(R.id.description) ;
        description.setText(meal.getDescription(getResources().getString(R.string.locale)));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
