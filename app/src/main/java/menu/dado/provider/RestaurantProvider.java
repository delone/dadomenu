package menu.dado.provider;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import menu.dado.model.MainCategory;
import menu.dado.model.Meal;
import menu.dado.model.MyOrder;
import menu.dado.model.NfcDataReader;
import menu.dado.model.Restaurant;
import menu.dado.model.RestaurantMenu;
import menu.dado.model.RestaurantService;
import menu.dado.model.SubCategory;
import menu.dado.sqliteDatabase.DatabaseHelper;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class RestaurantProvider  extends Application{
//    public static final String LOCALE_EN = "en" ;
//    public static final String LOCALE_AZ = "az" ;
    private static Restaurant restaurant ;
    private static MyOrder myOrder ;
    DatabaseHelper dbTestHelper = new DatabaseHelper();

    public static MyOrder getOrder()
    {
        if(myOrder == null)
        {
            myOrder = new MyOrder();
        }
        return myOrder ;
    }

    public static MyOrder createOrder()
    {
        myOrder = new MyOrder() ;
        return myOrder ;
    }

    public void emptyOrder()
    {
        if(myOrder != null)
        {
            myOrder = null;
        }
    }

    public static void instantiateOrder()
    {
        myOrder = new MyOrder();
    }

    public Restaurant getRestaurant(Context context)
    {
        String restId = NfcDataReader.getInstance().getRestId();
        if(myOrder == null)
        {
            myOrder = new MyOrder();
        }
        List<MainCategory> mainCategoryList = new ArrayList<MainCategory>();
        List<SubCategory> subCategoryList = new ArrayList<>();
        List<Meal> mealList = new ArrayList<>();
        List<RestaurantService> restaurantServiceList = new ArrayList<>();


        restaurant = dbTestHelper.selectRestaurant(context,restId);
        mainCategoryList = dbTestHelper.selectMainCategory(context,restId);
        subCategoryList = dbTestHelper.selectSubCategory(context,restId);
        mealList = dbTestHelper.selectMeal(context,restId);
        restaurantServiceList = dbTestHelper.selectExtraServices(context,restId);

        // Set Items for subCategory
        List<SubCategory> extraSubCategoryList = new ArrayList<SubCategory>();
        List<Meal> addMealList = null;
        for (SubCategory subCat : subCategoryList)
        {
            addMealList = new ArrayList<Meal>();
            for(Meal m: mealList)
            {
                if(subCat.getId() == m.getParentId())
                {
                    addMealList.add(m);
                }
            }
            Meal [] meals = new Meal[addMealList.size()];
            addMealList.toArray(meals);
            Log.d("ARRAYS", String.valueOf(meals.length));
            subCat.setItems(meals);
            extraSubCategoryList.add(subCat);
        }

        // Set Items for Main Category
        List<MainCategory> extraMainCategoryList = new ArrayList<MainCategory>();
        List<SubCategory> addSubList = null;
        for (MainCategory mainCat : mainCategoryList)
        {
            addSubList = new ArrayList<SubCategory>();
            for (SubCategory subCat : extraSubCategoryList)
            {
                if(mainCat.getId() == subCat.getParentId() && subCat.getItems().length>0)
                {
                   addSubList.add(subCat);
                }
            }
            SubCategory [] subs = new SubCategory[addSubList.size()];
            addSubList.toArray(subs);
            Log.d("ARRAYARRAY", String.valueOf(subs.length));
            mainCat.setItems(subs);
            extraMainCategoryList.add(mainCat);
        }



        RestaurantMenu restaurantMenu = new RestaurantMenu();
        restaurantMenu.setVersion(restaurant.getMenuVersion());
        MainCategory [] mains = new MainCategory[extraMainCategoryList.size()];
        extraMainCategoryList.toArray(mains);
        restaurantMenu.setCategories(mains);
//        for (MainCategory mainCategory : extraMainCategoryList)
//        {
//            restaurantMenu.setCategories(new MainCategory[]{mainCategory});
//        }
        restaurant.setMenu(restaurantMenu);

        RestaurantService[] extraservices = new RestaurantService[restaurantServiceList.size()];
        restaurantServiceList.toArray(extraservices);
        restaurant.setServices(extraservices);
//        for (RestaurantService restaurantService : restaurantServiceList)
//        {
//            restaurant.setServices(new RestaurantService[]{restaurantService});
//        }
        return restaurant;
    }

}
