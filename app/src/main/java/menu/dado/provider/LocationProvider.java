package menu.dado.provider;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by User on 01.07.2016.
 */
public class LocationProvider implements LocationListener {
    LocationManager locationManager;
    Location location;

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public Double getLastKnownLongitude(Context context)
    {
        if(location != null)
        {
            try
            {
                locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 10, this);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                Log.d("getLongitudeinyo", String.valueOf(location.getLongitude()));
            } catch (SecurityException e)
            {
            }
            return location.getLongitude();
        }
        else
        {
            return 0.0;
        }
    }

    public Double getLastKnownLatitude(Context context)
    {
        if(location != null)
        {
            try
            {
                locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 10, this);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                Log.d("ggetLatitudeinyo", String.valueOf(location.getLatitude()));
            } catch (SecurityException e)
            {
            }
            return location.getLatitude();
        }
        else
        {
            return 0.0;
        }
    }
}
