package menu.dado.provider;

import android.content.Context;
import android.content.SharedPreferences;

import menu.dado.R;

/**
 * Created by Android on 21.02.2016.
 */
public class User {
    public static int getUserId(Context context) {
        return getData(context).getInt("user_id", -1) ;
    }

    public static void setUserId(Context context, int userId) {
        SharedPreferences data = getData(context) ;
        SharedPreferences.Editor editor = data.edit();
        editor.putInt("user_id", userId);
        editor.commit();
    }

    private static SharedPreferences getData(Context context) {
        return context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE) ;
    }
}
