package menu.dado.provider;

import android.app.Activity;

/**
 * Created by User on 03.07.2016.
 */
public class MainActivityProvider
{
    public static MainActivityProvider mainActivityProvider;
    private Activity activity;

    private MainActivityProvider()
    {
        //
    }

    public static synchronized MainActivityProvider getInstance()
    {
        if(mainActivityProvider == null)
        {
            mainActivityProvider = new MainActivityProvider();
        }
        return mainActivityProvider;
    }

    public void clear()
    {
        mainActivityProvider = null;
    }

    public Activity getActivity()
    {
        return activity;
    }

    public void setActivity(Activity activity)
    {
        this.activity = activity;
    }
}
