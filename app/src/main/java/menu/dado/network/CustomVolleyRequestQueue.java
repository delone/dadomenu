package menu.dado.network;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import menu.dado.provider.AppContextProvider;

/**
 * Created by User on 30.06.2016.
 */
public class CustomVolleyRequestQueue
{
    private static CustomVolleyRequestQueue mInstance;
    private RequestQueue requestQueue;
    private static ImageLoader imageLoader;

    private CustomVolleyRequestQueue()
    {
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue,new ImageLoader.ImageCache()
        {
            private final LruCache<String,Bitmap> cache = new LruCache<String, Bitmap>(20);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url,bitmap);
            }
        });
    }

    public static synchronized CustomVolleyRequestQueue getInstance()
    {
        if(mInstance == null)
        {
            mInstance = new CustomVolleyRequestQueue();
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue()
    {
        if(requestQueue == null)
        {
            requestQueue = Volley.newRequestQueue(AppContextProvider.getContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request)
    {
        getRequestQueue().add(request);
    }

    public static ImageLoader getImageLoader()
    {
        return imageLoader;
    }
}
