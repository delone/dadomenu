package menu.dado.network;

/**
 * Created by User on 25.06.2016.
 */
public class Constants
{
    // Web Service urls
    public static String CoreUrl = "http://demo.mezozilla.net";
    public static String MakeOrderUrl = CoreUrl + "/dado/index.php?pg=29";
    public static String MakePaymentUrl = CoreUrl + "/dado/index.php?pg=30";
    public static String GetRestaurantInfoUrl = CoreUrl + "/dado/index.php?ln=az&pg=27";
    public static String GetRestaurantListUrl = CoreUrl + "/dado/index.php?pg=26";
    public static String GetCompanyListUrl = CoreUrl + "/dado/index.php?pg=25";
    public static String GetRestaurantVersionUrl = CoreUrl + "/dado/index.php?pg=28";
    public static String UserCreateUrl = CoreUrl + "/dado/index.php?pg=23";
    public static String MakeExtraServiceUrl = CoreUrl + "/dado/index.php?pg=32";

    // Api Paramaeters
    public static String UserId = "user_id";
    public static String CoordLatt = "cord_latt";
    public static String CoordLong = "cord_long";
    public static String OrderId = "order_id";
    public static String CompId = "comp_id";
    public static String PaymentType = "payment_type";
    public static String FeedbackScore = "feedback_score";
    public static String FeedbackComment = "feedback_comment";
    public static String OrderNote = "order_note";
    public static String OrderItems = "order_items";
    public static String RestId = "rest_id";
    public static String ManfId = "manf_id";
    public static String DadoId = "dado_id";
    public static String OrderStatus = "order_status";
    public static String ImsiId = "imsi_id";
    public static String ImeiId = "imei_id";
    public static String ExtraServices = "extra_services";

    // Restaurant fields
    public static String Name = "Name";
    public static String Phone = "Phone";
    public static String Description = "Description";
    public static String Address = "Address";
    public static String ImageUrl = "ImageUrl";
    public static String En = "en";
    public static String Az = "az";

    // Shared Preferences Keys
    public static String GpsLatitude = "Latitude";
    public static String GpsLongitude = "Longitude";
    public static String LastKnownLocation = "LastKnownLocation";

    // Shared Preferences File Names
    public static String UserIdSavedFileName = "UserIdSavedFileName";
    public static String RegisteredUserId = "RegisteredUserId";
    public static String ManfIdSavedFileName = "ManfIdSavedFileName";
    public static String ManfIdKey= "ManfId";
    public static String DadoIdSavedFileName = "DadoIdSavedFileName";
    public static String DadoIdKey = "DadoId";

    // AlerDialog Texts
    public static String AlertDialogTitle = "Dado Menu";
    public static String AlertDialogNetworkMessage = "No Internet Connection";
    public static String AlertDialogExtraServiceMessageUnable = "You don't have any order.Please order smth in order to make extra service request.";
    public static String AlertDialogExtraServiceMessageAble = "You successfully ordered extra service.";
    public static String AlertDialogWelcomeMessage = "Welcome.Tap to Nfc tag once again in order to see menu";

    // Restaurant Default Details
    public static String RestaurantDefaultName = "Name is not specified";
    public static String RestaurantDefaultDescription = "Description is not specified";
    public static String RestaurantDefaultAddress = "Address is not specified";
    public static String RestaurantDefaultPhone = "Phone is not specified";
    public static String RestaurantDefaultImageUrl = "http://url";
}
