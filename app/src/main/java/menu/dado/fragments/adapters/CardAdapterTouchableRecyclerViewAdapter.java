package menu.dado.fragments.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import at.markushi.ui.SolidCircleButton;
import it.gmariotti.cardslib.library.Constants;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.BaseRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.view.CardViewNative;
import menu.dado.R;
import menu.dado.fragments.extra.MealAddDialog;
import menu.dado.fragments.extra.Utility;
import menu.dado.model.MealCard;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.RestaurantProvider;

/**
 * Created by rdavudov on 1/24/2016.
 */
public class CardAdapterTouchableRecyclerViewAdapter extends CardArrayRecyclerViewAdapter implements Filterable {
    private MealFilter filter ;

    public CardAdapterTouchableRecyclerViewAdapter(Context context, List<Card> cards) {
        super(context, cards);
        this.context = context ;
    }

    private Context context ;
    @Override
    public BaseRecyclerViewAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseRecyclerViewAdapter.CardViewHolder holder = super.onCreateViewHolder(parent, viewType);
        final CardViewNative cardView = (CardViewNative) holder.mCardView ;
        cardView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (Build.VERSION.SDK_INT >= Constants.API_L) {
                            cardView.animate().setDuration(100).scaleX(1.01f).scaleY(1.01f).translationZ(10);
                        } else {
                            cardView.animate().setDuration(100).scaleX(1.01f).scaleY(1.01f);
                        }
                        return false;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        if (Build.VERSION.SDK_INT >= Constants.API_L) {
                            cardView.animate().setDuration(100).scaleX(1).scaleY(1).translationZ(0);
                        } else {
                            cardView.animate().setDuration(100).scaleX(1).scaleY(1);
                        }
                        return false;
                }
                return false;
            }
        });

        return holder ;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        final CardViewNative cardView = (CardViewNative) holder.mCardView ;
        final MealCard meal = (MealCard) cardView.getCard() ;
        TextView title = (TextView) cardView.findViewById(R.id.title) ;
        title.setText(meal.getMeal().getName(AppContextProvider.getContext().getResources().getString(R.string.locale)));
        TextView price = (TextView) cardView.findViewById(R.id.price) ;
        price.setText(meal.getMeal().getPrice() + " AZN");
        TextView description = (TextView) cardView.findViewById(R.id.description) ;
        String desc = meal.getMeal().getDescription(AppContextProvider.getContext().getResources().getString(R.string.locale)) ;
        if (desc != null && desc.length() > 0){
            description.setText(meal.getMeal().getDescription(AppContextProvider.getContext().getResources().getString(R.string.locale)));
            description.setVisibility(View.VISIBLE);
        }

        SolidCircleButton addButton = (SolidCircleButton) cardView.findViewById(R.id.btn_add) ;
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new MealAddDialog(context, meal.getMeal()).createDialog().show();
            }
        });
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new MealFilter() ;
        }
        return filter;
    }

    private class MealFilter extends Filter {
        protected FilterResults performFiltering(CharSequence query) {
            FilterResults result = new FilterResults();
            if(query == null || query.length() < 2){
                result.values = Utility.searchCards((Activity) context) ;
                result.count =  getCards().size() ;
            } else {
                ArrayList<MealCard> filtered = new ArrayList<MealCard>() ;
                for (Card item : getCards()) {
                    MealCard meal = (MealCard) item ;
                    int dist = calcDistance(meal.getMeal().getName("en").toLowerCase(Locale.ENGLISH), query.toString().toLowerCase(Locale.ENGLISH)) ;
                    if (dist > -1 ) {
                        meal.setWeight(dist); ;
                        filtered.add(meal) ;
                    }
                }
                Collections.sort(filtered, new Comparator<MealCard>() {
                    public int compare(MealCard lhs, MealCard rhs) {
                        return lhs.getWeight() - rhs.getWeight();
                    }
                }) ;

                result.values = filtered ;
                result.count = filtered.size() ;
            }

            return result;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            setCards((ArrayList<Card>) results.values);
            notifyDataSetChanged();
        }

        public int calcDistance(String s1, String s2) {
            String[] ss = s1.split(" ") ;
            for (int i = 0; i < ss.length; i++) {
                if (ss[i].contains(s2)) {
                    return 0 ;
                } else {
                    int dist = levensteinDistance(ss[i], s2) ;
                    if (dist < 3) {
                        return dist ;
                    }
                }
            }
            return -1 ;
        }

        public int levensteinDistance(String a, String b) {
            int [] costs = new int [b.length() + 1];
            for (int j = 0; j < costs.length; j++)
                costs[j] = j;
            for (int i = 1; i <= a.length(); i++) {
                costs[0] = i;
                int nw = i - 1;
                for (int j = 1; j <= b.length(); j++) {
                    int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                    nw = costs[j];
                    costs[j] = cj;
                }
            }
            return costs[b.length()];
        }
    }
}
