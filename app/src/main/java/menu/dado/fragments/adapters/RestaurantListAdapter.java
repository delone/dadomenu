package menu.dado.fragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import menu.dado.R;
import menu.dado.model.Restaurant;
import menu.dado.network.CustomVolleyRequestQueue;

/**
 * Created by User on 30.06.2016.
 */
public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.ViewHolder> {
    private Restaurant[] restaurants;

    public RestaurantListAdapter(Restaurant[] restaurants) {
        this.restaurants = restaurants ;
    }

    @Override
    public RestaurantListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RestaurantListAdapter.ViewHolder viewHolder, int i) {

        String imgUrl = (restaurants[i].getIconLarge().length()<=0)?"http://url":restaurants[i].getIconSmall(); // URL of the image
        //String imgUrl = "http://url"; // URL of the image

        ImageLoader imageLoader = CustomVolleyRequestQueue.getImageLoader();
        imageLoader.get(imgUrl,ImageLoader.getImageListener(viewHolder.restaurant_icon,R.drawable.pizza,R.drawable.pizza));
        viewHolder.restaurant_icon.setImageUrl(imgUrl,imageLoader);

        viewHolder.restaurant_name.setText(restaurants[i].getName("en"));
        viewHolder.restaurant_address.setText(restaurants[i].getAddress());
    }

    @Override
    public int getItemCount() {
        return restaurants.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private NetworkImageView restaurant_icon;
        private TextView restaurant_name;
        private TextView restaurant_address;

        public ViewHolder(View view) {
            super(view);
            restaurant_icon = (NetworkImageView) view.findViewById(R.id.restaurant_icon);
            restaurant_name = (TextView) view.findViewById(R.id.restaurant_name);
            restaurant_address = (TextView) view.findViewById(R.id.restaurant_address);
        }
    }
}
