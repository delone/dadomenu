package menu.dado.fragments.extra;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import menu.dado.activities.DetailActivity;
import menu.dado.model.MainCategory;
import menu.dado.model.Meal;
import menu.dado.model.MealCard;
import menu.dado.model.SubCategory;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.RestaurantProvider;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class Utility {
    public static final int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static ArrayList<Card> initCards(final Activity activity, long mainCategoryId, int position) {
        //Init an array of Cards
        ArrayList<Card> cards = new ArrayList<Card>();
        MainCategory mainCategory = new RestaurantProvider().getRestaurant(AppContextProvider.getContext()).getMenu().getMainCategory(mainCategoryId) ;
        SubCategory subCategory = null;
        if(mainCategory.getItems() != null)
        {
            subCategory = mainCategory.getItems()[position];
            if(subCategory.getItems() != null)
            {
                for (final Meal meal: subCategory.getItems()) {
                    Card card = new MealCard(activity, meal);
                    card.setOnClickListener(new Card.OnCardClickListener() {
                        @Override
                        public void onClick(Card card, View view) {
                            Intent intent = new Intent(activity, DetailActivity.class) ;
                            intent.putExtra("meal", meal) ;
                            activity.startActivity(intent) ;
                        }
                    });

                    cards.add(card);
                }
            }
        }

        return cards;
    }

    public static ArrayList<Card> searchCards(final Activity activity) {
        //Init an array of Cards
        ArrayList<Card> cards = new ArrayList<Card>();
        ArrayList<Meal> allMeals = new RestaurantProvider().getRestaurant(AppContextProvider.getContext()).getMenu().getAllMeals() ;

        for (final Meal meal: allMeals) {
            Card card = new MealCard(activity, meal);
            card.setOnClickListener(new Card.OnCardClickListener() {
                @Override
                public void onClick(Card card, View view) {
                    Intent intent = new Intent(activity, DetailActivity.class) ;
                    intent.putExtra("meal", meal) ;
                    activity.startActivity(intent) ;
                }
            });

            cards.add(card);
        }
        return cards;
    }
}
