package menu.dado.fragments.extra;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import menu.dado.R;
import menu.dado.model.MyOrder;
import menu.dado.model.NfcDataReader;
import menu.dado.model.RestaurantService;
import menu.dado.network.Constants;
import menu.dado.network.CustomVolleyRequestQueue;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.LocationProvider;
import menu.dado.provider.RestaurantProvider;

/**
 * Created by Android on 25.01.2016.
 */
public class ExtraServiceDialog {
    private Context context ;
    RequestQueue requestQueue;
    SharedPreferences sharedPreferences;

    public ExtraServiceDialog(Context context) {
        this.context = context ;
    }

    public AlertDialog createDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.dialog_services, null) ;

        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_services_title);
        builder.setView(v) ;

        final AlertDialog dialog = builder.create() ;
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface d) {
                final int titleDividerId = context.getResources().getIdentifier("titleDivider", "id", "android");
                final View titleDivider = dialog.findViewById(titleDividerId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                }
            }
        }) ;

        final ListView list = (ListView) v.findViewById(android.R.id.list) ;
        list.setAdapter(new ServicesAdapter(context, new RestaurantProvider().getRestaurant(AppContextProvider.getContext()).getServices()));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ServicesAdapter servicesAdapter = new ServicesAdapter(context,new RestaurantProvider().getRestaurant(AppContextProvider.getContext()).getServices());
                callExtraServices(servicesAdapter.getItemId(position));
                Log.d("bababab", String.valueOf(servicesAdapter.getItemId(position)));
                dialog.dismiss();
            }
        });
        return dialog ;
    }

    public class ServicesAdapter extends BaseAdapter {
        private RestaurantService[] services ;
        private Context context ;

        public ServicesAdapter(Context context, RestaurantService[] services) {
            this.context = context ;
            this.services = services ;
        }

        @Override
        public int getCount() {
            return services.length;
        }

        @Override
        public Object getItem(int position) {
            return services[position];
        }

        @Override
        public long getItemId(int position) {
            return services[position].getId() ;
        }

        class ViewHolder {
            TextView service;
            ImageView icon ;
        }

        ViewHolder holder;

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_service, null);
                holder = new ViewHolder();
                holder.service = (TextView) convertView.findViewById(R.id.service);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.service.setText(services[position].getName(AppContextProvider.getContext().getResources().getString(R.string.locale)));
//            holder.price.setText(portions.get(position).getPrice() + " AZN");
            return convertView;
        }
    }

    public void callExtraServices(long serviceId)
    {
        //TODO: Rest id has been read from Nfc and has been written to NfcdataReader instance fields
        NfcDataReader nfcDataReader = NfcDataReader.getInstance();
        //nfcDataReader.getRestId();

        sharedPreferences = AppContextProvider.getContext().getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);
        Log.d("useridinyo",sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        JSONObject params = new JSONObject();

        JSONObject extraServices = new JSONObject();
        try {
            extraServices.put(String.valueOf(serviceId),"1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // the POST parameters:
        final MyOrder order = RestaurantProvider.getOrder() ;
        if(order != null && order.getOrderId() != 0)
        {
            Log.d("zazaza", String.valueOf(order.getOrderId()));
            String orderId = String.valueOf(order.getOrderId());
            Log.d("lalalal",orderId);

            try {
                params.put(Constants.UserId,sharedPreferences.getString(Constants.RegisteredUserId,"1"));
                params.put(Constants.CoordLatt, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
                params.put(Constants.CoordLong, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
                params.put(Constants.RestId, "40");
                params.put(Constants.OrderId, orderId);
                params.put(Constants.ExtraServices, extraServices);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, Constants.MakeExtraServiceUrl,params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try
                            {
                                if(response.getString("error_code").equals("0"))
                                {
                                    Log.d("extraserviceeeee",response.toString());
                                    Toast.makeText(AppContextProvider.getContext(),Constants.AlertDialogExtraServiceMessageAble,Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error instanceof NoConnectionError || error instanceof NetworkError || error instanceof TimeoutError)
                            {
                                Toast.makeText(AppContextProvider.getContext(),Constants.AlertDialogNetworkMessage,Toast.LENGTH_LONG).show();
                            }
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "   application/json; charset=utf-8");
                    return headers;
                }
            };
            requestQueue = CustomVolleyRequestQueue.getInstance().getRequestQueue();
            requestQueue.add(postRequest);
        }
        else
        {
            Toast.makeText(AppContextProvider.getContext(),Constants.AlertDialogExtraServiceMessageUnable,Toast.LENGTH_LONG).show();

        }
    }
}
