package menu.dado.fragments.extra;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import at.markushi.ui.CircleButton;
import at.markushi.ui.SolidCircleButton;
import menu.dado.R;
import menu.dado.fragments.MyOrderFragment;
import menu.dado.model.Meal;
import menu.dado.model.MyOrder;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.MainActivityProvider;
import menu.dado.provider.RestaurantProvider;

/**
 * Created by Android on 25.01.2016.
 */
public class MealAddDialog {
    private Meal meal ;
    private HashMap<String, Integer> countMap ;
    private double totalPrice ;
    DecimalFormat df;
    private Context context ;

    private TextView totalText ;

    public MealAddDialog(Context context, Meal meal) {
        this.context = context ;
        this.meal = meal ;
        MyOrder myOrder = RestaurantProvider.getOrder() ;
        if (myOrder.hasItem(meal)) {
            countMap = myOrder.getItem(meal).getCountMap() ;
        } else {
            countMap = new HashMap<String, Integer>() ;
            totalPrice = 0;
        }
    }

    public double setTotalPrice() {
        double totalPrice = 0 ;
        int counts = 0 ;
        for (Map.Entry<String, Integer> entry:countMap.entrySet()) {
            totalPrice += meal.getPrice(entry.getKey()) * entry.getValue();
        }
        df = new DecimalFormat("#.00");
        totalText.setText(df.format(totalPrice) + " AZN");
        return totalPrice ;
    }

    public AlertDialog createDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.dialog_add, null) ;

        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(meal.getName(AppContextProvider.getContext().getResources().getString(R.string.locale)));
        builder.setView(v) ;
//        builder.setPositiveButton(R.string.dialog_meal_ok, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                MyOrderFragment myOrderFragment = (MyOrderFragment) ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag("MYORDER");
//                myOrderFragment.updateItem(meal, countMap);
//            }
//        }) ;
//        builder.setNegativeButton(R.string.dialog_meal_cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                JsonParser parser = new JsonParser();
//                Restaurant restaurant = parser.parse(context);
//                Toast.makeText(context, restaurant.getName("en"), Toast.LENGTH_SHORT).show();
//            }
//        }) ;

        final AlertDialog dialog = builder.create() ;
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface d) {
                final int titleDividerId = context.getResources().getIdentifier("titleDivider", "id", "android");
                final View titleDivider = dialog.findViewById(titleDividerId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                }
            }
        }) ;

        SolidCircleButton okBtn = (SolidCircleButton) v.findViewById(R.id.btn_ok) ;
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Log.d("OKBTNINYO",meal.getName("en"));
                Log.d("OKBTNINYO", String.valueOf(countMap.size()));
//                MainActivity mainActivity = (MainActivity) AppContextProvider.getContext();
//                FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
//                MyOrderFragment myOrderFragment = (MyOrderFragment) fragmentManager.findFragmentByTag("MYORDER");

                MainActivityProvider mainActivityProvider = MainActivityProvider.getInstance();
                Activity activity = mainActivityProvider.getActivity();
                MyOrderFragment myOrderFragment = (MyOrderFragment) ((FragmentActivity) activity).getSupportFragmentManager().findFragmentByTag("MYORDER");
                myOrderFragment.updateItem(meal, countMap);
            }
        });

        SolidCircleButton cancelBtn = (SolidCircleButton) v.findViewById(R.id.btn_cancel) ;
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        totalText = (TextView) v.findViewById(R.id.total) ;
        setTotalPrice() ;
        ListView list = (ListView) v.findViewById(android.R.id.list) ;
        list.setAdapter(new PortionAdapter(context, meal));
        return dialog ;
    }

    public class PortionAdapter extends BaseAdapter {
        private ArrayList<Meal.Portion> portions ;
        private Context context ;

        public PortionAdapter(Context context, Meal meal) {
            this.context = context ;
            portions = meal.getPortions() ;
        }

        @Override
        public int getCount() {
            return portions.size();
        }

        @Override
        public Object getItem(int position) {
            return portions.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position ;
        }

        class ViewHolder {
            TextView portion;
            TextView price;
            TextView count ;
            CircleButton del ;
            CircleButton add ;
        }

        ViewHolder holder;

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_portion, null);
                holder = new ViewHolder();
                holder.portion = (TextView) convertView.findViewById(R.id.portion);
                holder.price = (TextView) convertView.findViewById(R.id.price);
                holder.count = (TextView) convertView.findViewById(R.id.count);
                holder.del = (CircleButton) convertView.findViewById(R.id.del);
                holder.add = (CircleButton) convertView.findViewById(R.id.add);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.portion.setText(portions.get(position).getPortion());
            holder.price.setText(portions.get(position).getPrice() + " AZN");
            if (countMap.containsKey(portions.get(position).getPortion())) {
                holder.count.setText("x" + countMap.get(portions.get(position).getPortion()));
                if (countMap.get(portions.get(position).getPortion()) > 0) {
                    holder.count.setTypeface(null, Typeface.BOLD);
                } else {
                    holder.count.setTypeface(null, Typeface.NORMAL);
                }
            } else {
                holder.count.setText("x0");
                holder.count.setTypeface(null, Typeface.NORMAL);
            }

            holder.del.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int count ;
                    if (countMap.containsKey(portions.get(position).getPortion())) {
                        count = countMap.get(portions.get(position).getPortion());
                    } else {
                        count = 0 ;
                    }
                    if (count > 0) {
                        count = count - 1;
                        countMap.put(portions.get(position).getPortion(), count);
                        totalPrice = totalPrice - meal.getPrice(portions.get(position).getPortion());
                        setTotalPrice() ;
                        notifyDataSetChanged();
                    }
                }
            }) ;

            holder.add.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int count ;
                    if (countMap.containsKey(portions.get(position).getPortion())) {
                        count = countMap.get(portions.get(position).getPortion());
                    } else {
                        count = 0 ;
                    }
                    //Toast.makeText(context, "clicked "+count, Toast.LENGTH_SHORT).show();
                    count = count + 1;
                    countMap.put(portions.get(position).getPortion(), count);
                    totalPrice = totalPrice + meal.getPrice(portions.get(position).getPortion());
                    holder.count.setText("x" + count);
                    setTotalPrice() ;
                    notifyDataSetChanged();
                }
            }) ;
            return convertView;
        }
    }
}
