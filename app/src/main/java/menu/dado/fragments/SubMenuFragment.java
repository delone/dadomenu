package menu.dado.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import menu.dado.R;
import menu.dado.fragments.adapters.CardAdapterTouchableRecyclerViewAdapter;
import menu.dado.fragments.extra.PagerSlidingTabStrip;
import menu.dado.fragments.extra.Utility;
import menu.dado.provider.RestaurantProvider;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class SubMenuFragment extends Fragment {
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private SubCategoryAdapter adapter ;
    private long mainCategoryId ;

    public SubMenuFragment() {
        setRetainInstance(true);
    }

    public long getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(long mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    public void selectTab(int tabIndex) {
        pager.setCurrentItem(tabIndex);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_menu, null);
        pager = (ViewPager) view.findViewById(R.id.pager) ;
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs) ;
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        pager.setCurrentItem(0);
        return view;
    }

    @Override
    public void onViewStateRestored (Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        adapter = new SubCategoryAdapter(getActivity());
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
        changeColor(Utility.getColor(getActivity(), R.color.colorPrimary));

        tabs.setOnTabReselectedListener(new PagerSlidingTabStrip.OnTabReselectedListener() {
            @Override
            public void onTabReselected(int position) {
                //Toast.makeText(getActivity(), "Tab reselected: " + position, Toast.LENGTH_SHORT).show();
            }
        });
        ((AppCompatActivity)getActivity()).getSupportActionBar().setElevation(0);
    }

    private void changeColor(int newColor) {
        tabs.setBackgroundColor(newColor);
//        mTintManager.setTintColor(newColor);

    }
    public class SubCategoryAdapter extends PagerAdapter {

        private Context context ;

        public SubCategoryAdapter(Context context) {
            super();
            this.context = context ;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return new RestaurantProvider().getRestaurant(getActivity()).getMenu().getMainCategory(mainCategoryId).getItems()[position].getName(getResources().getString(R.string.locale)) ;
        }

        @Override
        public int getCount()
        {
            return new RestaurantProvider().getRestaurant(getActivity()).getMenu().getMainCategory(mainCategoryId).getItems().length;
        }

        public Object instantiateItem(ViewGroup collection, int position) {
            LayoutInflater inflater = LayoutInflater.from(context);
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.fragment_meal_recycler_view, collection, false);

            ArrayList<Card> cards = Utility.initCards(getActivity(), mainCategoryId, position);

            CardAdapterTouchableRecyclerViewAdapter mCardArrayAdapter = new CardAdapterTouchableRecyclerViewAdapter(getActivity(), cards);

            //Staggered grid view
            CardRecyclerView mRecyclerView = (CardRecyclerView) layout.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(false);
            final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity()) ;
            mRecyclerView.setLayoutManager(mLinearLayoutManager);

            //Set the empty view
            if (mRecyclerView != null) {
                mRecyclerView.setAdapter(mCardArrayAdapter);
            }


            collection.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object view) {
            container.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View v, Object o) {
            return v == ((View) o);
        }
    }

}
