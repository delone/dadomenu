package menu.dado.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import menu.dado.R;
import menu.dado.fragments.adapters.CardAdapterTouchableRecyclerViewAdapter;
import menu.dado.fragments.extra.Utility;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class SearchFragment extends Fragment {
    private long mainCategoryId ;
    private CardRecyclerView mRecyclerView ;
    private CardAdapterTouchableRecyclerViewAdapter mCardArrayAdapter ;
    public SearchFragment() {
        setRetainInstance(true);
    }

    public long getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(long mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meal_recycler_view, null);
        ArrayList<Card> cards = Utility.searchCards(getActivity());

        mCardArrayAdapter = new CardAdapterTouchableRecyclerViewAdapter(getActivity(), cards);

        //Staggered grid view
        mRecyclerView = (CardRecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);
        final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity()) ;
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        //Set the empty view
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(mCardArrayAdapter);
        }

        return view;
    }

    private String lastSearch ;

    public void updateSearch(String search) {
        if (mCardArrayAdapter != null) {
            mCardArrayAdapter.getFilter().filter(search);
            lastSearch = search ;
        }
    }

    public String getLastSearch() {
        return lastSearch;
    }
}
