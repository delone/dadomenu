package menu.dado.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Process;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import menu.dado.activities.MainActivity;
import menu.dado.R;
import menu.dado.activities.RestaurantListActivity;
import menu.dado.json.JsonExtractor;
import menu.dado.model.MainCategory;
import menu.dado.model.NfcDataReader;
import menu.dado.model.RestaurantMenu;
import menu.dado.network.Constants;
import menu.dado.network.CustomVolleyRequestQueue;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.LocationProvider;
import menu.dado.provider.RestaurantProvider;
import menu.dado.sqliteDatabase.DatabaseConstants;
import menu.dado.sqliteDatabase.DatabaseHelper;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class MainMenuFragment extends ListFragment
{
    ProgressDialog loading;
    DatabaseHelper databaseHelper = new DatabaseHelper();
    JsonExtractor jsonExtractor = new JsonExtractor();
    RequestQueue requestQueue;
    SharedPreferences sharedPreferences;
    NfcDataReader nfcDataReader;

    private ListView mainCategoryList ;
    private MainMenuAdapter adapter ;

    public MainMenuFragment() {
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, null);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        RestaurantMenu menu = new RestaurantProvider().getRestaurant(getActivity()).getMenu();
        if(menu.getCategories().length<=0)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getResources().getString(R.string.no_menu_in_restaurant_message))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            dialog.dismiss();
                            Intent intent = new Intent(getActivity(), RestaurantListActivity.class) ;
                            startActivity(intent);
                            getActivity().finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        mainCategoryList = getListView() ;
        adapter = new MainMenuAdapter(menu) ;
        mainCategoryList.setAdapter(adapter);
        mainCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((MainActivity)getActivity()).initSubMenuFragment(id);
            }
        });
    }

    public class MainMenuAdapter extends BaseAdapter {
        private MainCategory[] mainMenuList ;

        public MainMenuAdapter(RestaurantMenu menu) {
            mainMenuList = menu.getCategories() ;
        }

        public void setMenu(RestaurantMenu menu) {
            mainMenuList = menu.getCategories() ;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mainMenuList.length;
        }

        @Override
        public Object getItem(int position) {
            return mainMenuList[position];
        }

        @Override
        public long getItemId(int position) {
            return mainMenuList[position].getId();
        }

        class ViewHolder {
            TextView text;
            NetworkImageView image;
        }

        ViewHolder holder;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_main_menu, null);
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.title);
                holder.image = (NetworkImageView) convertView.findViewById(R.id.icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.text.setText(mainMenuList[position].getName(getResources().getString(R.string.locale)));

            String imgUrl = (mainMenuList[position].getIconSmall().length()<=0)?"http://url":mainMenuList[position].getIconSmall();
            ImageLoader imageLoader = CustomVolleyRequestQueue.getImageLoader();
            imageLoader.get(imgUrl,ImageLoader.getImageListener(holder.image,R.drawable.pizza,R.drawable.pizza));
            holder.image.setImageUrl(imgUrl,imageLoader);

            return convertView;
        }
    }

    public void getRestaurantInfo(String restId)
    {
        //TODO: Rest id has been read from Nfc and has been written to NfcdataReader instance fields
        sharedPreferences = getActivity().getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);
        Log.d("useridinyo",sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        Map<String, String> params = new HashMap<String, String>();
        // the POST parameters:
        params.put(Constants.UserId,sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        params.put(Constants.CoordLatt, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.CoordLong, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
        params.put(Constants.RestId, restId);
        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Loading...","Please wait...",false,false);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, Constants.GetRestaurantInfoUrl,new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try
                        {
                            if(response.getString("error_code").equals("0"))
                            {
                                String menuVersion;
                                JSONObject restaurant = response.getJSONObject("restaurant");
                                menuVersion = (restaurant.isNull("menu_version")==true)?"000000987600":restaurant.getString("menu_version");
                                response.put("menu_version",menuVersion);
                                Log.d("RESPONSINYO","I AM HERE TO CHECK WHETHER MENU VERSION HAS CHANGED OR NOT");
                                if(databaseHelper.selectRestaurant(AppContextProvider.getContext(),nfcDataReader.getRestId()).getMenuVersion() == Long.parseLong(menuVersion))
                                {
                                    Log.d("CHECKINGYO","NO CHANGES WERE DETECTED IN MENU VERSION");
                                }
                                else
                                {
                                    Log.d("CHECKINGYO","A CHANGE WAS DETECTED IN MENU VERSION SO I AM GONNA CALL API OF RESTAURANT_INFO");

                                    databaseHelper.deleteRestaurant(AppContextProvider.getContext(),nfcDataReader.getRestId());
                                    databaseHelper.deleteExtraServices(AppContextProvider.getContext(),nfcDataReader.getRestId());
                                    databaseHelper.deleteMainCategory(AppContextProvider.getContext(),nfcDataReader.getRestId());
                                    databaseHelper.deleteSubCategory(AppContextProvider.getContext(),nfcDataReader.getRestId());
                                    databaseHelper.deleteMeal(AppContextProvider.getContext(),nfcDataReader.getRestId());

                                    databaseHelper.insertRestaurant(AppContextProvider.getContext(),jsonExtractor.extractRestaurant(response));
                                    databaseHelper.insertExtraServices(AppContextProvider.getContext(),jsonExtractor.extractExtraServices(response));
                                    databaseHelper.insertMainCategory(AppContextProvider.getContext(),jsonExtractor.extractMainCategory(response));
                                    databaseHelper.insertSubCategory(AppContextProvider.getContext(),jsonExtractor.extractSubCategory(response));
                                    databaseHelper.insertMeal(AppContextProvider.getContext(),jsonExtractor.extractMeal(response));
                                }

                                loading.dismiss();
                                RestaurantMenu menu = new RestaurantProvider().getRestaurant(getActivity()).getMenu();
                                if(menu.getCategories().length<=0)
                                {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(getResources().getString(R.string.no_menu_in_restaurant_message))
                                            .setCancelable(false)
                                            .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                                            {
                                                public void onClick(DialogInterface dialog, int id)
                                                {
                                                    dialog.dismiss();
                                                    Intent intent = new Intent(getActivity(), RestaurantListActivity.class) ;
                                                    startActivity(intent);
                                                    getActivity().finish();
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                                mainCategoryList = getListView() ;
                                adapter = new MainMenuAdapter(menu) ;
                                mainCategoryList.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                mainCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                                {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        ((MainActivity)getActivity()).initSubMenuFragment(id);
                                    }
                                });
                            }

                            else
                            {
                                // dismiss loading dialog first
                                loading.dismiss();

                                // show alertdialog to alert appropriate message
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(getResources().getString(R.string.no_such_restaurant_message))
                                        .setCancelable(false)
                                        .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int id)
                                            {
                                                dialog.dismiss();
                                                Intent intent = new Intent(getActivity(), RestaurantListActivity.class) ;
                                                startActivity(intent);
                                                getActivity().finish();
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        loading.dismiss();
                        if(error instanceof NoConnectionError || error instanceof NetworkError || error instanceof TimeoutError)
                        {
                            // dismiss loading dilog first


                            // show alertdialog to alert appropriate message
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(getResources().getString(R.string.no_internet_message))
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.alert_dialog_Ok), new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int id)
                                        {
                                            dialog.dismiss();
                                            getActivity().finish();
                                            Process.killProcess(Process.myPid());
                                            System.exit(0);
                                        }
                                    })
                                    .setNegativeButton(getResources().getString(R.string.alert_dialog_Retry), new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int id)
                                        {
                                            getRestaurantInfo(nfcDataReader.getRestId());
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

//                            RestaurantMenu menu = new RestaurantProvider().getRestaurant(getActivity()).getMenu();
//
//                            mainCategoryList = getListView() ;
//                            adapter = new MainMenuAdapter(menu) ;
//                            mainCategoryList.setAdapter(adapter);
//                            adapter.notifyDataSetChanged();
//                            mainCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener()
//                            {
//                                @Override
//                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                    ((MainActivity)getActivity()).initSubMenuFragment(id);
//                                }
//                            });
                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "   application/json; charset=utf-8");
                return headers;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(4 * 1000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue = CustomVolleyRequestQueue.getInstance().getRequestQueue();
        requestQueue.add(postRequest);
    }


}
