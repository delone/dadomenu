package menu.dado.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import menu.dado.activities.MainActivity;
import menu.dado.R;
import menu.dado.model.RestaurantMenu;
import menu.dado.model.SubCategory;
import menu.dado.network.CustomVolleyRequestQueue;
import menu.dado.provider.RestaurantProvider;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class SubMenuListFragment extends ListFragment {
    private ListView mainCategoryList ;
    private SubMenuAdapter adapter ;
    private SubMenuFragment subMenuFragment ;
    private MainActivity mainActivity ;
    public SubMenuListFragment() {
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_menu_list, null);
        return view;
    }

    public void setSubMenu(SubCategory[] subMenuList) {
        this.adapter.setSubMenu(subMenuList);
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public SubMenuFragment getSubMenuFragment() {
        return subMenuFragment;
    }

    public void setSubMenuFragment(SubMenuFragment subMenuFragment) {
        this.subMenuFragment = subMenuFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RestaurantMenu menu = new RestaurantProvider().getRestaurant(getActivity()).getMenu() ;
        mainCategoryList = getListView() ;
        adapter = new SubMenuAdapter() ;
        mainCategoryList.setAdapter(adapter);
        mainCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                subMenuFragment.selectTab(position);
                mainActivity.closeRightSideBar();
            }
        });
    }

    public class SubMenuAdapter extends BaseAdapter {
        private SubCategory[] subMenuList = new SubCategory[0];

        public SubMenuAdapter() {
        }

        public void setSubMenu(SubCategory[] subMenuList) {
            this.subMenuList = subMenuList ;
            notifyDataSetChanged(); ;
        }

        @Override
        public int getCount() {
            return subMenuList.length;
        }

        @Override
        public Object getItem(int position) {
            return subMenuList[position];
        }

        @Override
        public long getItemId(int position) {
            return subMenuList[position].getId();
        }

        class ViewHolder {
            TextView text;
            NetworkImageView image;
        }

        ViewHolder holder;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_main_menu, null);
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.title);
                holder.image = (NetworkImageView) convertView.findViewById(R.id.icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.text.setText(subMenuList[position].getName(getResources().getString(R.string.locale)));

            String imgUrl = (subMenuList[position].getIconSmall().length()<=0)?"http://url":subMenuList[position].getIconSmall();
            imgUrl = "http://url";
            ImageLoader imageLoader = CustomVolleyRequestQueue.getImageLoader();
            imageLoader.get(imgUrl,ImageLoader.getImageListener(holder.image,R.drawable.pizza,R.drawable.pizza));
            holder.image.setImageUrl(imgUrl,imageLoader);

            return convertView;
        }
    }

}
