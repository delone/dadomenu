package menu.dado.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import at.markushi.ui.RoundRectangleButton;
import at.markushi.ui.SolidCircleButton;
import menu.dado.R;
import menu.dado.activities.MainActivity;
import menu.dado.fragments.extra.MealAddDialog;
import menu.dado.json.JsonExtractor;
import menu.dado.model.ApiResult;
import menu.dado.model.Meal;
import menu.dado.model.MyOrder;
import menu.dado.model.OrderItem;
import menu.dado.network.Constants;
import menu.dado.network.CustomVolleyRequestQueue;
import menu.dado.provider.AppContextProvider;
import menu.dado.provider.LocationProvider;
import menu.dado.provider.MainActivityProvider;
import menu.dado.provider.RestaurantProvider;
import menu.dado.sqliteDatabase.DatabaseHelper;

/**
 * Created by rdavudov on 1/23/2016.
 */
public class MyOrderFragment extends ListFragment {
    DecimalFormat df;
    private boolean isLoading ;
    private AsyncTask task ;
    private OrderAdapter adapter ;
    private SharedPreferences sharedPreferences;
    private RequestQueue requestQueue;
//    private Order order ;

    private RoundRectangleButton btnClearAll ;
    private RoundRectangleButton btnRate ;
    private RoundRectangleButton btnPlaceOrder ;
    private RoundRectangleButton btnNewOrder ;
    private RoundRectangleButton btnPay ;
    private RoundRectangleButton btnPaySmall ;
    private TextView txtTotal ;
    private TextView txtDuration ;
    private ImageView txtDurationIcon ;
    private EditText txtMessage ;
    private SlidingMenu slidingMenu;

    DatabaseHelper databaseHelper = new DatabaseHelper();
    JsonExtractor jsonExtractor = new JsonExtractor();

    public MyOrderFragment() {
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        requestQueue = CustomVolleyRequestQueue.getInstance().getRequestQueue();
        View view = inflater.inflate(R.layout.fragment_my_order, null);

        txtTotal = (TextView) view.findViewById(R.id.order_total) ;
        txtDuration = (TextView) view.findViewById(R.id.order_duration) ;
        txtDurationIcon = (ImageView) view.findViewById(R.id.order_dur_icon) ;
        txtMessage = (EditText) view.findViewById(R.id.order_message) ;

        txtMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager imm = (InputMethodManager)
                            v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                } else {
                    Log.d("focus", "focused");
                }
            }
        });
        setTotalPrice();
        MyOrder order = RestaurantProvider.getOrder() ;
        if (order.getOrderMessage() != null && order.getOrderMessage().length() > 0) {
            txtMessage.setText(order.getOrderMessage()) ;
            Log.d("ORDERINYO",order.getOrderMessage());
        }

        btnClearAll = (RoundRectangleButton) view.findViewById(R.id.btn_order_clear) ;
        btnRate = (RoundRectangleButton) view.findViewById(R.id.btn_order_rate) ;
        btnPlaceOrder = (RoundRectangleButton) view.findViewById(R.id.btn_order_place) ;
        btnNewOrder = (RoundRectangleButton) view.findViewById(R.id.btn_order_new) ;
        btnPay = (RoundRectangleButton) view.findViewById(R.id.btn_order_pay) ;
        btnPaySmall = (RoundRectangleButton) view.findViewById(R.id.btn_order_pay_small) ;

        btnClearAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final MyOrder order = RestaurantProvider.getOrder() ;
                if (order.getItemCount() > 0) {
                    Runnable cancelAction = new Runnable() {
                        @Override
                        public void run() {
                            clearAllOrderItems() ;
                        }
                    };
                    initClearDialog(R.string.order_dialog_title_clear_all, cancelAction, null,order).show() ;
                } else {
                    clearAllOrderItems() ;
                }
            }
        }) ;


        btnRate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                final Dialog rankDialog = new Dialog(getActivity(), R.style.FullHeightDialog);
                rankDialog.setContentView(R.layout.rank_dialog);
                rankDialog.setCancelable(false);
                final RatingBar ratingBar = (RatingBar)rankDialog.findViewById(R.id.dialog_ratingbar);
                ratingBar.setNumStars(5);
                final MyOrder order = RestaurantProvider.getOrder() ;
                if(order.getFeedbackScore() == null)
                {
                    order.setFeedbackScore("0");
                }
                ratingBar.setRating(Float.parseFloat(order.getFeedbackScore()));

                TextView text = (TextView) rankDialog.findViewById(R.id.rank_dialog_text1);
                text.setText(getResources().getString(R.string.feedback_title));
                Button okButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rankDialog.dismiss();
                        ratingBar.setRating(ratingBar.getRating());
                        Log.d("sasasa", String.valueOf(ratingBar.getRating()));
                        final MyOrder order = RestaurantProvider.getOrder() ;
                        order.setFeedbackScore( String.valueOf(ratingBar.getRating()));
                    }
                });
                //now that the dialog is set up, it's time to show it
                rankDialog.show();
            }
        }) ;


        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final MyOrder order = RestaurantProvider.getOrder();
                //Toast.makeText(getContext(), "hasNewItem=" + order.hasNewItem(), Toast.LENGTH_SHORT).show();
                Log.d("txtmessage", String.valueOf(txtMessage.getText()));
                if (order.hasNewItem()) {
                    Runnable placeOrderAction = new Runnable() {
                        public void run() {
//                            isLoading = true ;
//                            getActivity().invalidateOptionsMenu() ;
//                            initButtonControls();
//                            task = new OrderPlaceTask().execute(order);
                        }
                    };

                    initConfirmationDialog(R.string.order_dialog_title_place_order, placeOrderAction, null,txtMessage).show() ;
                }

            }
        }) ;

        View.OnClickListener payClick = new View.OnClickListener() {
            public void onClick(View v) {
                Runnable payAction = new Runnable() {
                    @Override
                    public void run() {
//                        final MyOrder order = RestaurantProvider.getOrder() ;
//                        isLoading = true ;
//                        getActivity().invalidateOptionsMenu() ;
//                        initButtonControls() ;
//                        task = new OrderPayTask().execute(order) ;
                    }
                };
                final MyOrder order = RestaurantProvider.getOrder() ;
                initPaymentDialog(R.string.order_dialog_title_payment, payAction, payAction).show() ;
            }
        } ;

        btnPay.setOnClickListener(payClick) ;
        btnPaySmall.setOnClickListener(payClick) ;

        btnNewOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clearAllOrderItems() ;
                final MyOrder order = RestaurantProvider.getOrder() ;
                txtTotal.setText(getResources().getString(R.string.order_total) + String.format(" %.2f AZN", order.getTotalPrice())) ;
                txtMessage.setText("");
            }
        }) ;
        initButtonControls() ;

        view.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    if (txtMessage.isFocused()) {
                        Rect outRect = new Rect();
                        txtMessage.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int)event.getRawX(),(int)event.getRawY()))
                        {
                            txtMessage.clearFocus();
                            //
                            // Hide keyboard
                            //
                            InputMethodManager imm = (InputMethodManager)
                                    v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return true;
            }
        });

        return view ;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new OrderAdapter(getActivity()) ;
        Log.d("fafafa",getActivity().toString());
        setListAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ((SwipeLayout)(getListView().getChildAt(position - getListView().getFirstVisiblePosition()))).open(true);
            }
        });
//
//        getListView().setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Log.e("ListView", "OnTouch");
//                return false;
//            }
//        });
//        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getContext(), "OnItemLongClickListener", Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        });
//        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                Log.e("ListView", "onScrollStateChanged");
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//
//            }
//        });
//
//        getListView().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.e("ListView", "onItemSelected:" + position);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                Log.e("ListView", "onNothingSelected:");
//            }
//        });
    }

    public void setTotalPrice() {
        MyOrder order = RestaurantProvider.getOrder() ;
        if (order != null) {
            txtTotal.setText(getResources().getString(R.string.order_total) + String.format(" %.2f AZN", order.getTotalPrice())) ;
            Log.d("ORDER TOTAL PRICE", String.valueOf(order.getTotalPrice()));
        }
        if (order.getTotalDuration() > 0) {
            txtDurationIcon.setVisibility(View.VISIBLE) ;
            txtDuration.setVisibility(View.VISIBLE) ;
            txtDuration.setText(order.getTotalDuration() + " " + getResources().getString(R.string.order_duration_time)) ;
        } else {
            txtDurationIcon.setVisibility(View.INVISIBLE) ;
            txtDuration.setVisibility(View.INVISIBLE) ;
        }
    }

    public void clearAllOrderItems() {
        MyOrder order = RestaurantProvider.createOrder() ;
        setTotalPrice();
        adapter.notifyDataSetChanged();
        initButtonControls();
    }

    public void refreshOrder() {
        MyOrder order = RestaurantProvider.getOrder() ;
        if (order.getOrderId() > 0) {
            isLoading = true ;
            getActivity().invalidateOptionsMenu() ;
            initButtonControls() ;
            task = new OrderRefreshTask().execute(order) ;
        }
    }

    public void initButtonControls() {
        MyOrder order = RestaurantProvider.getOrder() ;
        btnRate.setVisibility(View.GONE);
        if (order.getOrderStatus() == MyOrder.STATUS_INITIAL) {
            btnClearAll.setVisibility(View.VISIBLE) ;
            btnPlaceOrder.setVisibility(View.VISIBLE) ;
            btnPlaceOrder.setText(R.string.order_btn_place_order) ;
            btnNewOrder.setVisibility(View.GONE) ;
            btnPay.setVisibility(View.GONE) ;
            btnPaySmall.setVisibility(View.GONE) ;
            txtMessage.setEnabled(true) ;
        } else if (order.getOrderStatus() == MyOrder.STATUS_ORDERED) {

            if (order.hasNewItem()) {
                btnClearAll.setVisibility(View.VISIBLE) ;
                btnPlaceOrder.setVisibility(View.VISIBLE) ;
                btnPlaceOrder.setText(R.string.order_btn_update_order) ;
                txtMessage.setEnabled(true) ;
                btnPaySmall.setVisibility(View.GONE) ;
                btnPay.setVisibility(View.VISIBLE) ;
            } else {
                btnClearAll.setVisibility(View.GONE) ;
                btnRate.setVisibility(View.VISIBLE) ;
                btnPlaceOrder.setVisibility(View.GONE) ;
                txtMessage.setEnabled(false) ;
                btnPaySmall.setVisibility(View.VISIBLE) ;
                btnPay.setVisibility(View.GONE) ;
                txtMessage.setEnabled(true);
            }
            btnNewOrder.setVisibility(View.GONE) ;
        } else if (order.getOrderStatus() == MyOrder.STATUS_PAID) {
            btnNewOrder.setVisibility(View.VISIBLE) ;
            txtMessage.setEnabled(false) ;
            btnClearAll.setVisibility(View.GONE) ;
            btnPlaceOrder.setVisibility(View.GONE) ;
            btnPay.setVisibility(View.GONE) ;
            btnPaySmall.setVisibility(View.GONE) ;
        } else {
            btnClearAll.setVisibility(View.GONE) ;
            btnNewOrder.setVisibility(View.GONE) ;
            btnPlaceOrder.setVisibility(View.GONE) ;
            btnPay.setVisibility(View.GONE) ;
            btnPaySmall.setVisibility(View.GONE) ;
            txtMessage.setEnabled(false) ;
        }

        if (isLoading) {
            btnClearAll.setColor(getResources().getColor(R.color.order_disabled)) ;
            btnClearAll.setClickable(false) ;
            btnPlaceOrder.setColor(getResources().getColor(R.color.order_disabled)) ;
            btnPlaceOrder.setClickable(false) ;
            btnNewOrder.setColor(getResources().getColor(R.color.order_disabled)) ;
            btnNewOrder.setClickable(false) ;
            btnPay.setColor(getResources().getColor(R.color.order_disabled)) ;
            btnPay.setClickable(false) ;
            btnPaySmall.setColor(getResources().getColor(R.color.order_disabled)) ;
            btnPaySmall.setClickable(false) ;
        } else {
            btnClearAll.setColor(getResources().getColor(R.color.order_clear)) ;
            btnClearAll.setClickable(true) ;
            btnPlaceOrder.setColor(getResources().getColor(R.color.order_create)) ;
            btnPlaceOrder.setClickable(true) ;
            btnNewOrder.setColor(getResources().getColor(R.color.order_new)) ;
            btnNewOrder.setClickable(true) ;
            btnPay.setColor(getResources().getColor(R.color.order_pay)) ;
            btnPay.setClickable(true) ;
            btnPaySmall.setColor(getResources().getColor(R.color.order_pay)) ;
            btnPaySmall.setClickable(true) ;
        }
    }

    public class OrderAdapter extends BaseSwipeAdapter
    {
        private Context context ;
        public OrderAdapter(Context context)
        {
            super();
            this.context = context ;
        }

        class ViewHolder
        {
            TextView title;
            TextView description ;
            TextView price ;
            SolidCircleButton modify ;
            SolidCircleButton remove ;
        }

        ViewHolder holder;

        @Override
        public int getSwipeLayoutResourceId(int position)
        {
            return R.id.swipe;
        }

        @Override
        public View generateView(final int position, ViewGroup parent)
        {
            final View convertView = LayoutInflater.from(context).inflate(R.layout.list_item_order, null);

                holder = new ViewHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.description = (TextView) convertView.findViewById(R.id.description);
                holder.price = (TextView) convertView.findViewById(R.id.price);
                holder.modify = (SolidCircleButton) convertView.findViewById(R.id.btn_modify);
                holder.remove = (SolidCircleButton) convertView.findViewById(R.id.btn_remove);
                convertView.setTag(holder);



            Log.d("POSITION", String.valueOf(position));

            SwipeLayout swipeLayout = (SwipeLayout)convertView.findViewById(getSwipeLayoutResourceId(position));
            swipeLayout.addSwipeListener(new SimpleSwipeListener() {
                @Override
                public void onOpen(SwipeLayout layout) {
                }
            });



//            swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
//                @Override
//                public void onDoubleClick(SwipeLayout layout, boolean surface) {
//                    Toast.makeText(context, "DoubleClick", Toast.LENGTH_SHORT).show();
//                }
//            });
            convertView.findViewById(R.id.btn_remove).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    mItemManger.removeShownLayouts(swipeLayout);
//                    orderlist.remove(position);
//                    notifyItemRemoved(position);
//                    notifyItemRangeChanged(position, studentList.size());
//                    mItemManger.closeAllItems();
                    final MyOrder order = RestaurantProvider.getOrder() ;
                    OrderItem orderItem;
                    Meal meal;
                   orderItem = RestaurantProvider.getOrder().getItem(position);
                        meal = orderItem.getMeal();
                        order.removeItem(meal);
                    adapter.notifyDataSetChanged();
                    txtTotal.setText(getResources().getString(R.string.order_total) + String.format(" %.2f AZN", order.getTotalPrice())) ;
                    txtMessage.setText("");
                }
            });

            return convertView;
        }

        @Override
        public void fillValues(int position, View convertView)
        {
            holder = (ViewHolder) convertView.getTag();
            final OrderItem item =  RestaurantProvider.getOrder().getItem(position);
            holder.title.setText(item.getMeal().getName(getResources().getString(R.string.locale)) + " x" + item.getCount() + "  ");
            df = new DecimalFormat("#.00");
            df.format(item.getPrice());
            holder.price.setText(df.format(item.getPrice()) + " AZN");
            if (item.getCountMap().size() > 1)
            {
                String portionText = item.getPortionText() ;
                holder.description.setText(portionText);
                holder.description.setVisibility(View.VISIBLE);
            }
            else
            {
                Log.d("lalalafo","I am invisible because of getcountmap");
                holder.description.setVisibility(View.GONE);
            }
            if (item.isLocked() == false)
            {
                holder.modify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new MealAddDialog(getContext(), item.getMeal()).createDialog().show();
                    }
                });
                holder.modify.setClickable(true);
                holder.modify.setColor(ContextCompat.getColor(getContext(), R.color.yes_color)) ;
                holder.modify.setImageResource(R.drawable.menu_ic_edit);

                holder.remove.setClickable(true);
                holder.remove.setColor(ContextCompat.getColor(getContext(),R.color.no_color));
                holder.remove.setImageResource(R.drawable.menu_ic_cancel);
            }
            else
            {
                convertView.setEnabled(false);
                holder.modify.setClickable(false);
                holder.modify.setColor(ContextCompat.getColor(getContext(), R.color.no_color)) ;
                holder.modify.setImageResource(R.drawable.menu_ic_lock);

                holder.remove.setClickable(false);
                holder.remove.setColor(ContextCompat.getColor(getContext(), R.color.no_color)) ;
                holder.remove.setImageResource(R.drawable.menu_ic_lock);
            }



            Log.d("dadadadadad","i fillvalues");
        }


        @Override
        public int getCount() {
            return RestaurantProvider.getOrder().getItemCount();
        }

        @Override
        public Object getItem(int position) {
            return RestaurantProvider.getOrder().getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return RestaurantProvider.getOrder().getItem(position).getMealId() ;
        }
    }

    public void updateItem(Meal meal, HashMap<String, Integer> countMap) {
        Log.d("UPDATEITEMINYO", String.valueOf(countMap));
        Log.d("mamamama", String.valueOf(RestaurantProvider.getOrder().getItemCount()));
        int prevoius = RestaurantProvider.getOrder().getItemCount();
        RestaurantProvider.getOrder().updateItem(meal, countMap);
        int current = RestaurantProvider.getOrder().getItemCount();
        if(prevoius == 0 && current ==1 )
        {
            ((MainActivity)getActivity()).initSlidingMenuForFragment();
        }
        Log.d("mamamama", String.valueOf(RestaurantProvider.getOrder().getItemCount()));
        setTotalPrice();
        adapter.notifyDataSetChanged();
        initButtonControls();
    }

    public boolean isLoading() {
        return isLoading;
    }

    public class OrderRefreshTask extends AsyncTask<MyOrder, Void, ApiResult> {
        protected ApiResult doInBackground(MyOrder... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            ApiResult result = new ApiResult() ;
            return result;
        }

        protected void onPostExecute(final ApiResult result) {
            isLoading = false ;
            initButtonControls() ;
            getActivity().invalidateOptionsMenu() ;
            task = null;
        }

        @Override
        protected void onCancelled() {
            task = null;
        }
    }

    public class OrderPlaceTask extends AsyncTask<MyOrder, Void, ApiResult> {
        private boolean isFirstUpdate ;
        private MyOrder order ;

        protected ApiResult doInBackground(MyOrder... params) {
            try {
                Thread.sleep(5000) ;
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            order = params[0] ;
            if (order.getOrderId() == 0) {
                order.setOrderId(new Random().nextInt(10000)); ;
                isFirstUpdate = true ;
            }
            ApiResult result = new ApiResult() ;
            return result;
        }

        protected void onPostExecute(final ApiResult result) {
            order.setOrderStatus(MyOrder.STATUS_ORDERED); ;
            order.lockItems(); ;
            isLoading = false ;
            initButtonControls() ;
            getActivity().invalidateOptionsMenu() ;
            adapter.notifyDataSetChanged();
            task = null;
        }

        @Override
        protected void onCancelled() {
            task = null;
        }
    }

    public class OrderPayTask extends AsyncTask<MyOrder, Void, ApiResult> {
        private MyOrder order ;
        protected ApiResult doInBackground(MyOrder... params) {
            try {
                Thread.sleep(5000) ;
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            order = params[0] ;
            ApiResult result = new ApiResult() ;
            return result;
        }

        protected void onPostExecute(final ApiResult result) {
            order.setOrderStatus(MyOrder.STATUS_PAID);
            isLoading = false ;
            initButtonControls() ;
            getActivity().invalidateOptionsMenu() ;
            task = null;
        }

        @Override
        protected void onCancelled() {
            task = null;
        }
    }

    private AlertDialog initConfirmationDialog(int titleId, final Runnable action1, final Runnable action2, final EditText txtMessage)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_confirmation, null) ;

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle(titleId);
        builder.setView(v) ;
        final AlertDialog dialog = builder.create() ;
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface d) {
                final int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
                final View titleDivider = dialog.findViewById(titleDividerId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        }) ;

        final SolidCircleButton btnOk = (SolidCircleButton) v.findViewById(R.id.btn_ok) ;

        final SolidCircleButton btnCancel = (SolidCircleButton) v.findViewById(R.id.btn_cancel) ;

        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (action1 != null) {
                    //action1.run() ;
                    isLoading = true ;
                    getActivity().invalidateOptionsMenu() ;
                    initButtonControls() ;
                    Log.d("RUNNABLE","I AM RUNNABLE");
                    try {
                        makeOrder(String.valueOf(txtMessage.getText()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                dialog.dismiss() ;
            }
        }) ;
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (action2 != null) {
                    action2.run() ;
                }
                dialog.dismiss() ;
            }
        }) ;

        return dialog ;
    }

    private AlertDialog initClearDialog(int titleId, final Runnable action1, final Runnable action2, final MyOrder order)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_confirmation, null) ;

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle(titleId);
        builder.setView(v) ;
        final AlertDialog dialog = builder.create() ;
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface d) {
                final int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
                final View titleDivider = dialog.findViewById(titleDividerId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        }) ;

        final SolidCircleButton btnOk = (SolidCircleButton) v.findViewById(R.id.btn_ok) ;

        final SolidCircleButton btnCancel = (SolidCircleButton) v.findViewById(R.id.btn_cancel) ;

        btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (action1 != null) {
                    //action1.run() ;
                    isLoading = true ;
                    getActivity().invalidateOptionsMenu() ;
                    initButtonControls() ;
                    Log.d("RUNNABLE","I AM RUNNABLE");
                    try
                    {
                        Log.d("lalalalalalala","clear here ");
                        final MyOrder order = RestaurantProvider.getOrder() ;
                        OrderItem orderItem;
                        Meal meal;
                        while (order.getItemCount()>=1)
                        {
                            orderItem = RestaurantProvider.getOrder().getItem(order.getItemCount()-1);
                            meal = orderItem.getMeal();
                            order.removeItem(meal);
                        }
                        //txtTotal.setText("Total: 0.00 AZN");
                        txtTotal.setText(getResources().getString(R.string.order_total) + String.format(" %.2f AZN", order.getTotalPrice())) ;
                        txtMessage.setText("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // lock clear
                    isLoading = false;
                    initButtonControls() ;
                    getActivity().invalidateOptionsMenu() ;
                    adapter.notifyDataSetChanged();
                }
                dialog.dismiss() ;
            }
        }) ;
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (action2 != null) {
                    action2.run() ;
                }
                dialog.dismiss() ;
            }
        }) ;

        return dialog ;
    }


    private AlertDialog initPaymentDialog(int titleId, final Runnable action1, final Runnable action2) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_payment, null) ;

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle(titleId);
        builder.setView(v) ;
        final AlertDialog dialog = builder.create() ;
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface d) {
                final int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
                final View titleDivider = dialog.findViewById(titleDividerId);
                if (titleDivider != null) {
                    titleDivider.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        }) ;

        final SolidCircleButton btnCash = (SolidCircleButton) v.findViewById(R.id.btn_cash) ;

        final SolidCircleButton btnCard = (SolidCircleButton) v.findViewById(R.id.btn_card) ;

        btnCash.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (action1 != null) {
                    //action1.run() ;
                    isLoading = true ;
                    getActivity().invalidateOptionsMenu() ;
                    initButtonControls() ;
                    try
                    {
                        makePayment("cash");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                dialog.dismiss() ;
            }
        }) ;
        btnCard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (action2 != null) {
                    //action2.run() ;
                    isLoading = true ;
                    getActivity().invalidateOptionsMenu() ;
                    initButtonControls() ;
                    try {
                        makePayment("card");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                dialog.dismiss() ;
            }
        }) ;

        return dialog ;
    }

    public void makeOrder(String orderNote) throws JSONException {
        // Order item-leri Restaurant providerden orderi get elemekle elde edirem
        // beleki hemin order obyekteden orderItemleri secirem..onlarin quantity ve portionlarini elde edib gonderirem
        sharedPreferences = AppContextProvider.getContext().getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);
        Log.d("useridinyo",sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        final MyOrder order = RestaurantProvider.getOrder() ;
        OrderItem orderItem = null;
        JSONObject portionsJson = new JSONObject();
        JSONObject mealId = new JSONObject();
        for(int i=0;i<order.getItemCount();i++)
        {
            // portions value ve quantity listlerini arraya cevirirem ki eyni loopun altinda oxuyum
            orderItem = RestaurantProvider.getOrder().getItem(i);
            List<String> portionList = orderItem.getPortionValue();
            List<Integer> quantityList = orderItem.getPortionQuantity();
            String[] portions = new String[ portionList.size() ];
            portionList.toArray(portions);
            Integer[] quantities = new Integer[ quantityList.size() ];
            quantityList.toArray(quantities);
            for (int j=0;j<portions.length;j++)
            {
                portionsJson.put(portions[j],String.valueOf(quantities[j]));
            }
            mealId.put(String.valueOf(orderItem.getMeal().getId()),portionsJson);
            // Log.d("ORDERIDINYO", String.valueOf(order.getOrderId()));
        }
        JSONArray jsonArray = new JSONArray();
        JSONObject params = new JSONObject();
        try
        {
            params.put(Constants.UserId, sharedPreferences.getString(Constants.RegisteredUserId,"1"));// Mandatory
            params.put(Constants.CoordLatt, String.valueOf(new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
            params.put(Constants.CoordLong, String.valueOf(new LocationProvider().getLastKnownLongitude(AppContextProvider.getContext())));
            params.put(Constants.ManfId, "33D113E9"); // Mandatory
            params.put(Constants.DadoId, "798189514868"); // Mandatory
            params.put(Constants.OrderId,"0");
            params.put(Constants.OrderNote, orderNote);
            params.put(Constants.OrderItems, mealId);
        }
        catch (JSONException js){Log.e("JONEXCEPTION",js.getMessage());}
        Log.d("paramsinyo", String.valueOf(params));
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, Constants.MakeOrderUrl,params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try
                        {
                            Log.d("ORDERINYO","I AM HERE TO ORDER THE MEAL " + response.toString());
                            MyOrder myOrder = jsonExtractor.extractOrder(response);
                            Log.d("ORDERID", String.valueOf(myOrder.getOrderId()));
                            order.setOrderId(myOrder.getOrderId());
                            Log.d("responsinyo",response.toString());
                            if(response.getString("order_status").equals("0") == false)
                            {
                                Toast.makeText(getActivity(),getResources().getString(R.string.successfull_order_message),Toast.LENGTH_LONG).show();
                                Log.d("toastinyo","i am here");
                                order.setOrderStatus(MyOrder.STATUS_ORDERED);
                                order.lockItems();
                                txtMessage.setText("");
                            }
                        } catch (Exception e)
                        {
                            Toast.makeText(getActivity(),getResources().getString(R.string.failed_order_message),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                        isLoading = false ;
                        initButtonControls() ;
                        getActivity().invalidateOptionsMenu() ;
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof NoConnectionError || error instanceof NetworkError || error instanceof TimeoutError)
                        {
                            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                            alertDialog.setTitle(getResources().getString(R.string.alert_dialog_title));
                            alertDialog.setMessage(getResources().getString(R.string.no_internet_message));
                            //alertDialog.setIcon(R.drawable.icon);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.alert_dialog_Ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                        isLoading = false ;
                        initButtonControls() ;
                        getActivity().invalidateOptionsMenu() ;
                        adapter.notifyDataSetChanged();
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "   application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(postRequest);
    }

    public void makePayment(String paymentType) throws JSONException {
        sharedPreferences = AppContextProvider.getContext().getSharedPreferences(Constants.UserIdSavedFileName, Context.MODE_PRIVATE);
        Log.d("useridinyo",sharedPreferences.getString(Constants.RegisteredUserId,"1"));
        final MyOrder order = RestaurantProvider.getOrder() ;
        JSONObject params = new JSONObject();
        try
        {
            params.put(Constants.UserId, sharedPreferences.getString(Constants.RegisteredUserId,"1"));
            params.put(Constants.CoordLatt, String.valueOf( new LocationProvider().getLastKnownLatitude(AppContextProvider.getContext())));
            params.put(Constants.CoordLong, String.valueOf( new LocationProvider().getLastKnownLongitude(AppContextProvider.getContext())));
            params.put(Constants.OrderId, order.getOrderId());
            params.put(Constants.PaymentType,paymentType);
            params.put(Constants.FeedbackScore, order.getFeedbackScore());
            params.put(Constants.FeedbackComment, txtMessage.getText().toString());
        }
        catch (JSONException js){Log.e("JONEXCEPTION",js.getMessage());}

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, Constants.MakePaymentUrl,params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try
                        {
                            Log.d("PAYINYO","I AM HERE TO PAY " + response.toString());
                            Log.d("PAYINYO","I AM HERE TO PAY " + order.getOrderId() + order.getOrderStatus());
                            Log.d("papapa",response.toString());
                            if(response.getString("error_mess").length() >0)
                            {
                                Toast.makeText(getActivity(),getResources().getString(R.string.unconfirmed_payment_message),Toast.LENGTH_LONG).show();
                                order.setOrderStatus(MyOrder.STATUS_ORDERED);
                            }
                            else
                            {
                                Toast.makeText(getActivity(),getResources().getString(R.string.successfull_payment_message),Toast.LENGTH_LONG).show();
                                order.setOrderStatus(MyOrder.STATUS_PAID);
                            }
                            order.lockItems();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        isLoading = false ;
                        initButtonControls() ;
                        getActivity().invalidateOptionsMenu() ;
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof NoConnectionError || error instanceof NetworkError || error instanceof TimeoutError)
                        {
                            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                            alertDialog.setTitle(getResources().getString(R.string.alert_dialog_title));
                            alertDialog.setMessage(getResources().getString(R.string.no_internet_message));
                            //alertDialog.setIcon(R.drawable.icon);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.alert_dialog_Ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                        isLoading = false ;
                        initButtonControls() ;
                        getActivity().invalidateOptionsMenu() ;
                        adapter.notifyDataSetChanged();
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "   application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(postRequest);
    }
}
