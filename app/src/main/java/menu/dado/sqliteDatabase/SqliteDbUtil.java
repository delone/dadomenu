package menu.dado.sqliteDatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by User on 19.06.2016.
 */
public class SqliteDbUtil
{
    public void closeResaources(SQLiteDatabase sqLiteDatabase, Cursor cursor)
    {
        if(sqLiteDatabase != null)
        {
            sqLiteDatabase.close();
        }
        if(cursor != null)
        {
            cursor.close();
        }
    }
}
