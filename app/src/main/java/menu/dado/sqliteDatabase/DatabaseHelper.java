package menu.dado.sqliteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import menu.dado.model.MainCategory;
import menu.dado.model.Meal;
import menu.dado.model.NfcDataReader;
import menu.dado.model.Restaurant;
import menu.dado.model.RestaurantService;
import menu.dado.model.SubCategory;

/**
 * Created by User on 18.06.2016.
 */
public class DatabaseHelper
{
    //private Context context;
    SQLiteDatabase sqLiteDatabase = null;
    SqliteDbUtil sqliteDbUtil = new SqliteDbUtil();

    // TODO:/ Create queries
    public  void createDatabaseRestaurantMenu(Context context)
    {
        // Create Database
        try
        {
            Log.d("CREATINYO","I AM HERE TO CREATE DatabaseRestaurantMenu TABLES");
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            // Create Tables in the database
            sqLiteDatabase.execSQL(DatabaseConstants.CreateMainCategory);
            sqLiteDatabase.execSQL(DatabaseConstants.CreateSubCatgory);
            sqLiteDatabase.execSQL(DatabaseConstants.CreateMeal);
            sqLiteDatabase.execSQL(DatabaseConstants.CreateRestaurant);
            sqLiteDatabase.execSQL(DatabaseConstants.CreateExtraServices);
            Log.d("CREATINYO","I CREATED  DATABASE RestaurantMenu AND ITS TABLES SUCCESSFULLY");
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    // TODO:/ Insert queries

    public void insertRestaurant(Context context,Restaurant restaurant)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            // Insert into Tables
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.Id, NfcDataReader.getInstance().getRestId());
            contentValues.put(DatabaseConstants.ImageSmall,restaurant.getIconSmall());
            contentValues.put(DatabaseConstants.ImageLarge,restaurant.getIconLarge());
            contentValues.put(DatabaseConstants.DescriptionEn,restaurant.getDescription("en"));
            contentValues.put(DatabaseConstants.DescriptionAz,restaurant.getDescription("az"));
            contentValues.put(DatabaseConstants.NameEn,restaurant.getName("en"));
            contentValues.put(DatabaseConstants.NameAz,restaurant.getName("az"));
            contentValues.put(DatabaseConstants.MenuVersion,restaurant.getMenuVersion());
            contentValues.put(DatabaseConstants.Telephone,restaurant.getPhones());
            contentValues.put(DatabaseConstants.Address,restaurant.getAddress());
            contentValues.put(DatabaseConstants.CoordinateLatitude,restaurant.getGpsLatitude());
            contentValues.put(DatabaseConstants.CoordinateLongitude,restaurant.getGpsLongitude());

            sqLiteDatabase.insert(DatabaseConstants.TableRestaurant,null,contentValues);
            Log.d("INSERTINYO","I INSERTED RESTAURANT TO RESTAURANT TABLE  " + restaurant.getName("az") + restaurant.getIconLarge() + restaurant.getId());
            Log.d("INSERTINYO","I INSERTED RESTAURANT TO RESTAURANT TABLE  " + restaurant.getMenuVersion());
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void insertExtraServices(Context context, List<RestaurantService> restaurantServiceList)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);

            for (RestaurantService restaurantService : restaurantServiceList)
            {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseConstants.Id,restaurantService.getId());
                contentValues.put(DatabaseConstants.RestId,NfcDataReader.getInstance().getRestId());
                contentValues.put(DatabaseConstants.NameEn,restaurantService.getName("en"));
                contentValues.put(DatabaseConstants.NameAz,restaurantService.getName("az"));
                sqLiteDatabase.insert(DatabaseConstants.TableExtraServices,null,contentValues);
                Log.d("INSERTINYO","I INSERTED EXTRASERVICES SUCCESSFULLY  " + restaurantService.getName("en") + restaurantService.getId() + restaurantService.getLocalText());
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void insertMainCategory(Context context,List<MainCategory> mainCategoryList)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);

            for (MainCategory mainCategory : mainCategoryList)
            {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseConstants.Id,mainCategory.getId());
                contentValues.put(DatabaseConstants.RestId,NfcDataReader.getInstance().getRestId());
                contentValues.put(DatabaseConstants.ImageSmall,mainCategory.getIconSmall());
                contentValues.put(DatabaseConstants.ImageLarge,mainCategory.getIconLarge());
                contentValues.put(DatabaseConstants.DescriptionEn,mainCategory.getDescription("en"));
                contentValues.put(DatabaseConstants.DescriptionAz,mainCategory.getDescription("az"));
                contentValues.put(DatabaseConstants.NameEn,mainCategory.getName("en"));
                contentValues.put(DatabaseConstants.NameAz,mainCategory.getName("az"));
                sqLiteDatabase.insert(DatabaseConstants.TableMainCategory,null,contentValues);
                Log.d("INSERTINYO","I INSERTED MAINCATEGORY SUCCESSFULLY  " + mainCategory.getName("en") + mainCategory.getId());
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void insertSubCategory(Context context, List<SubCategory> subCategoryList)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);

            for (SubCategory subCategory : subCategoryList)
            {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseConstants.Id,subCategory.getId());
                contentValues.put(DatabaseConstants.RestId,NfcDataReader.getInstance().getRestId());
                contentValues.put(DatabaseConstants.ParenId,subCategory.getParentId());
                Log.d("PARENTINYO", String.valueOf(subCategory.getParentId()));
                contentValues.put(DatabaseConstants.ImageSmall,subCategory.getIconSmall());
                contentValues.put(DatabaseConstants.ImageLarge,subCategory.getIconLarge());
                contentValues.put(DatabaseConstants.DescriptionEn,subCategory.getDescription("en"));
                contentValues.put(DatabaseConstants.DescriptionAz,subCategory.getDescription("az"));
                contentValues.put(DatabaseConstants.NameEn,subCategory.getName("en"));
                contentValues.put(DatabaseConstants.NameAz,subCategory.getName("az"));
                sqLiteDatabase.insert(DatabaseConstants.TableSubCategory,null,contentValues);
                Log.d("INSERTINYO","I INSERTED SUBCATEGORY SUCCESSFULLY " + subCategory.getName("en") + subCategory.getIconLarge() + subCategory.getIconSmall());
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void insertMeal(Context context, List<Meal> mealList)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);

            for (Meal meal : mealList)
            {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseConstants.Id,meal.getId());
                contentValues.put(DatabaseConstants.RestId,NfcDataReader.getInstance().getRestId());
                contentValues.put(DatabaseConstants.ParenId,meal.getParentId());
                contentValues.put(DatabaseConstants.ImageSmall,meal.getIconSmall());
                contentValues.put(DatabaseConstants.ImageLarge,meal.getIconLarge());
                contentValues.put(DatabaseConstants.DescriptionEn,meal.getDescription("en"));
                contentValues.put(DatabaseConstants.DescriptionAz,meal.getDescription("az"));
                contentValues.put(DatabaseConstants.NameEn,meal.getName("en"));
                contentValues.put(DatabaseConstants.NameAz,meal.getName("az"));
                contentValues.put(DatabaseConstants.Price,meal.getPrice());
                contentValues.put(DatabaseConstants.PrepareTime,meal.getPreparationTime());
                contentValues.put(DatabaseConstants.Portions,meal.getPortionsString());
                Log.d("insertinyoportions",meal.getPortionsString());
                sqLiteDatabase.insert(DatabaseConstants.TableMeal,null,contentValues);
                Log.d("INSERTINYO","I INSERTED MEALS SUCCESSFULLY  " + meal.getName("en") + meal.getId());
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    // TODO:/ Delete queries

    public void deleteRestaurant(Context context,String restId)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            sqLiteDatabase.execSQL(DatabaseConstants.DeleteRestaurant + restId);
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void deleteExtraServices(Context context,String restId)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            sqLiteDatabase.execSQL(DatabaseConstants.DeleteExtraServices + restId);
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void deleteMainCategory(Context context,String restId)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            sqLiteDatabase.execSQL(DatabaseConstants.DeleteMainCategory + restId);
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void deleteSubCategory(Context context,String restId)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            sqLiteDatabase.execSQL(DatabaseConstants.DeleteSubCategory + restId);
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    public void deleteMeal(Context context,String restId)
    {
        try
        {
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            sqLiteDatabase.execSQL(DatabaseConstants.DeleteMeal + restId);
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            if(sqLiteDatabase != null)
            {
                sqLiteDatabase.close();
            }
        }
    }

    // TODO:/ Select queries

    public List<RestaurantService> selectExtraServices(Context context,String restId)
    {
        Cursor cursor = null;
        List<RestaurantService> restaurantServiceList = new ArrayList<RestaurantService>();
        try
        {
            // Retrieve from Tables
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            cursor = sqLiteDatabase.rawQuery(DatabaseConstants.SelectExtraServices + restId,null);
            if(cursor != null)
            {
                while (cursor.moveToNext())
                {
                    RestaurantService restaurantService = new RestaurantService();
                    restaurantService.setId(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.Id)));
                    restaurantService.setName( "en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)) == null)? "": cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)));
                    restaurantService.setName( "az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)) == null)? "": cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)));
                    Log.d("SELECTINYO","I SELECTED EXTRASERVICES " + restaurantService.getName("en") + restaurantService.getId());
                    restaurantServiceList.add(restaurantService);
                }
                Log.d("SELECTINYO", String.valueOf(cursor.getCount()));
            }
        }
        catch (Exception e)
        {
            Log.d("ERROR",e.getMessage());
        }
        finally
        {
            sqliteDbUtil.closeResaources(sqLiteDatabase,cursor);
        }
        return restaurantServiceList;
    }

    public List<MainCategory> selectMainCategory(Context context,String restId)
    {
        Cursor cursor = null;
        List<MainCategory> mainCategoryList = new ArrayList<MainCategory>();
        try
        {
            // Retrieve from Tables
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            cursor = sqLiteDatabase.rawQuery(DatabaseConstants.SelectMainCategory + restId,null);
            while (cursor.moveToNext())
                {
                    MainCategory mainCategory  = new MainCategory();
                    mainCategory.setId(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.Id)));
                    mainCategory.setIconSmall( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)) == null)? "": cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)));
                    mainCategory.setIconLarge( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)) == null) ? "" : cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)));
                    mainCategory.setName("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)) == null ) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)));
                    mainCategory.setName("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)));
                    mainCategory.setDescription("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)));
                    mainCategory.setDescription("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)));
                    Log.d("SELECTINYO","I SELECTED MAIN CATEGORY " + mainCategory.getIconLarge() + mainCategory.getName("en") + mainCategory.getId());
                    // addto list
                    mainCategoryList.add(mainCategory);
                }
                Log.d("SELECTINYO", String.valueOf(mainCategoryList.size()));

        }
        catch (Exception e)
        {
            Log.d("ERROR",e.getMessage());
        }
        finally
        {
           sqliteDbUtil.closeResaources(sqLiteDatabase,cursor);
        }
        return mainCategoryList;
    }

    public List<SubCategory> selectSubCategory(Context context,String restId)
    {
        Cursor cursor = null;
        List<SubCategory> subCategoryList = new ArrayList<SubCategory>();
        try
        {
            // Retrieve from Tables
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            cursor = sqLiteDatabase.rawQuery(DatabaseConstants.SelectSubCategory + restId,null);
            if(cursor != null)
            {
                Log.d("cursorsize", String.valueOf(cursor.getCount()));
                while (cursor.moveToNext())
                {
                    SubCategory subCategory = new SubCategory();
                    subCategory.setId(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.Id)));
                    subCategory.setParentId(cursor.getLong(cursor.getColumnIndex(DatabaseConstants.ParenId)));
                    subCategory.setIconSmall( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)) == null)? "": cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)));
                    subCategory.setIconLarge( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)) == null) ? "" : cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)));
                    subCategory.setName("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)) == null ) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)));
                    subCategory.setName("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)));
                    subCategory.setDescription("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)));
                    subCategory.setDescription("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)));
                    Log.d("MESSAGE", String.valueOf(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.Id))));
                    Log.d("SELECTINYO","I SELECTED SUBCATEGORIES THAT YOU WANTED FROM ME " + subCategory.getId() + subCategory.getName("en"));
                    // addto list
                    subCategoryList.add(subCategory);
                    Log.d("SELECTINYO", String.valueOf(cursor.getCount()));
                }
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }
        finally
        {
            sqliteDbUtil.closeResaources(sqLiteDatabase,cursor);
        }
        return subCategoryList;
    }

    public List<Meal> selectMeal(Context context,String restId)
    {
        List<Meal> mealList = new ArrayList<Meal>();
        Cursor cursor = null;
        try
        {
            // Retrieve from Tables
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            cursor = sqLiteDatabase.rawQuery(DatabaseConstants.SelectMeal + restId,null);
            if(cursor != null)
            {
                while (cursor.moveToNext())
                {
                    Meal meal = new Meal();
                    meal.setId(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.Id)));
                    meal.setParentId(cursor.getLong(cursor.getColumnIndex(DatabaseConstants.ParenId)));
                    meal.setIconSmall( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)) == null)? "": cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)));
                    meal.setIconLarge( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)) == null) ? "" : cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)));
                    meal.setName("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)) == null ) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)));
                    meal.setName("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)));
                    meal.setDescription("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)));
                    meal.setDescription("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)));
                    meal.setPrice(Double.parseDouble((cursor.getString(cursor.getColumnIndex(DatabaseConstants.Price)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.Price))));
                    meal.setPreparationTime(Integer.parseInt((cursor.getString(cursor.getColumnIndex(DatabaseConstants.PrepareTime)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.PrepareTime))));

                    if(cursor.getString(cursor.getColumnIndex(DatabaseConstants.Portions)).length()<=0)
                    {
                        //meal.addPortion("1",meal.getPrice());
                        Log.d("Noportions","sdasda");
                    }
                    else
                    {
                        JSONObject portions = new JSONObject(String.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseConstants.Portions))));
                        //JSONObject portions = cursor.getColumnIndex(DatabaseConstants.Portions);
                        Iterator keys = portions.keys();
                        while (keys.hasNext())
                        {
                            String currentKey = (String) keys.next();
                            String currentKeyValue = portions.getString(currentKey);
                            Log.d("portionsKeys",currentKey + " : " + currentKeyValue);
                            meal.addPortion(currentKey, Double.parseDouble(currentKeyValue));
                        }
                        Log.d("Yesportions",String.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseConstants.Portions))));
                    }
                    Log.d("SELECTINYO","I SELECTED WHAT YOU WANTED FROM MEALS " + meal.getId() + meal.getName("en"));
//                    // addto list
                    mealList.add(meal);
                }
            }
        }
        catch (Exception e)
        {
            Log.d("ERROR",e.getMessage());
        }
        finally
        {
            sqliteDbUtil.closeResaources(sqLiteDatabase,cursor);
        }
        return mealList;
    }

    public Restaurant selectRestaurant(Context context,String restId)
    {
        Restaurant restaurant = new Restaurant();
        Cursor cursor = null;
        try
        {
            // Retrieve from Tables
            sqLiteDatabase = context.openOrCreateDatabase(DatabaseConstants.DatabaseRestaurantMenu, Context.MODE_PRIVATE, null);
            cursor = sqLiteDatabase.rawQuery(DatabaseConstants.SelectRestaurant + restId,null);

            cursor.moveToFirst();
            if(cursor != null)
            {
                restaurant.setId(cursor.getInt(cursor.getColumnIndex(DatabaseConstants.Id)));
                restaurant.setIconSmall( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)) == null)? "": cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageSmall)));
                restaurant.setIconLarge( (cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)) == null) ? "" : cursor.getString(cursor.getColumnIndex(DatabaseConstants.ImageLarge)));
                restaurant.setName("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)) == null ) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameEn)));
                restaurant.setName("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.NameAz)));
                restaurant.setDescription("en",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionEn)));
                restaurant.setDescription("az",(cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.DescriptionAz)));
                restaurant.setMenuVersion(Long.parseLong((cursor.getString(cursor.getColumnIndex(DatabaseConstants.MenuVersion)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.MenuVersion))));
                restaurant.setPhones((cursor.getString(cursor.getColumnIndex(DatabaseConstants.Telephone)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.Telephone)));
                restaurant.setAddress((cursor.getString(cursor.getColumnIndex(DatabaseConstants.Address)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.Address)));
                restaurant.setGpsLatitude(Double.parseDouble((cursor.getString(cursor.getColumnIndex(DatabaseConstants.CoordinateLatitude)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.CoordinateLatitude))));
                restaurant.setGpsLongitude(Double.parseDouble((cursor.getString(cursor.getColumnIndex(DatabaseConstants.CoordinateLongitude)) == null) ? "" :cursor.getString(cursor.getColumnIndex(DatabaseConstants.CoordinateLongitude))));

                Log.d("SELECTINYO", "I SELECTED RESTAURANT " + restaurant.getIconLarge() + restaurant.getPhones() + restaurant.getId() + restaurant.getName("az") + restaurant.getAddress());
            }
            else
            {
                Log.d("SELECTINYO", "I am in the else ");
            }
        }
        catch (Exception e)
        {
            Log.d("ERROR",e.getMessage());
        }
        finally
        {
            sqliteDbUtil.closeResaources(sqLiteDatabase,cursor);
        }
        return restaurant;
    }

    // TODO: Check table and DB

    public boolean checkDataBase(Context context) {
        File database=context.getDatabasePath("RestaurantMenu.db");
        return database.exists();
    }
}
