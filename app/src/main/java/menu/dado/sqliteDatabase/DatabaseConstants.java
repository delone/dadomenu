package menu.dado.sqliteDatabase;

import menu.dado.model.Restaurant;

/**
 * Created by User on 18.06.2016.
 */
public class DatabaseConstants
{
    // Names
    public static final String DatabaseRestaurantMenu = "RestaurantMenu";
    public static final String TableMainCategory = "MainCategory";
    public static final String TableSubCategory = "SubCategory";
    public static final String TableMeal = "Meal";
    public static final String TableExtraServices = "ExtraServices";
    public static final String TableRestaurant = "Restaurant";

    public static final String Id = "id";
    public static final String RestId = "restId";
    public static final String CompanyId = "companyid";
    public static final String ParenId = "parent_id";
    public static final String ImageSmall = "image_small";
    public static final String ImageLarge = "image_large";
    public static final String NameEn = "name";
    public static final String NameAz = "ad";
    public static final String DescriptionEn = "descriptionEn";
    public static final String DescriptionAz = "descriptionAz";
    public static final String Price = "price";
    public static final String PrepareTime = "preparetime";
    public static final String Portions = "portions";
    public static final String MenuVersion = "menu_version";
    public static final String Telephone = "telephone";
    public static final String Address = "address";
    public static final String CoordinateLatitude = "coordinate_latt";
    public static final String CoordinateLongitude = "coordinate_long";



    /* Queries */

    public static final String CreateMainCategory = "CREATE TABLE IF NOT EXISTS " + TableMainCategory
            + "(" + Id + " INTEGER," +RestId +" INTEGER," + ImageSmall
            + " VARCHAR," + ImageLarge + " VARCHAR," + DescriptionEn
            + " VARCHAR," + DescriptionAz + " VARCHAR,"+  NameEn + " VARCHAR," +  NameAz + " VARCHAR" + ")";

    public static final String CreateSubCatgory = "CREATE TABLE IF NOT EXISTS " + TableSubCategory
            + "(" + Id + " INTEGER ,"  +RestId +" INTEGER," + ParenId + " INTEGER ," +  ImageSmall
            + " VARCHAR," + ImageLarge + " VARCHAR," + DescriptionEn
            + " VARCHAR," + DescriptionAz + " VARCHAR,"+  NameEn + " VARCHAR," +  NameAz + " VARCHAR" + ")";

    public static final String CreateMeal = "CREATE TABLE IF NOT EXISTS " + TableMeal + "(" + Id + " INTEGER ," +RestId +" INTEGER," + ParenId + " INTEGER ,"
            + ImageSmall + " VARCHAR," + ImageLarge + " VARCHAR," + DescriptionEn + " VARCHAR," + DescriptionAz + " VARCHAR,"+  NameEn
            + " VARCHAR," +  NameAz + " VARCHAR," + Price + " VARCHAR," + PrepareTime + " VARCHAR," + Portions + " VARCHAR" +  ")";

    public static final String CreateRestaurant = "CREATE TABLE IF NOT EXISTS " + TableRestaurant + "(" + Id + " INTEGER,"
            + ImageSmall + " VARCHAR," + ImageLarge + " VARCHAR," + DescriptionEn + " VARCHAR," + DescriptionAz + " VARCHAR,"+  NameEn
            + " VARCHAR," +  NameAz + " VARCHAR," + MenuVersion + " VARCHAR," + Telephone + " VARCHAR," + Address + " VARCHAR," +
            CoordinateLatitude + " VARCHAR," + CoordinateLongitude + " VARCHAR" + ")";

    public static final String CreateExtraServices = "CREATE TABLE IF NOT EXISTS " + TableExtraServices + "(" + Id + " INTEGER,"
          + RestId + " INTEGER," +  NameEn + " VARCHAR," +  NameAz + " VARCHAR" + ")";

    // Select Queries
    public static final String SelectMainCategory = "SELECT * FROM " + TableMainCategory + " where " + RestId + " = ";
    public static final String SelectSubCategory = "SELECT * FROM " + TableSubCategory + " where " + RestId + " = ";
    public static final String SelectMeal = "SELECT * FROM " + TableMeal + " where " + RestId + " = ";
    public static final String SelectRestaurant = "SELECT * FROM " + TableRestaurant + " where " + Id + " = ";
    public static final String SelectExtraServices = "SELECT * FROM " + TableExtraServices + " where " + RestId + " = ";

    // Delete Queries
    public static final String DeleteRestaurant = "DELETE FROM " + TableRestaurant + " where " + Id + " = ";
    public static final String DeleteMainCategory = "DELETE FROM " + TableMainCategory + " where " + RestId + " = ";
    public static final String DeleteSubCategory = "DELETE FROM " + TableSubCategory + " where " + RestId + " = ";
    public static final String DeleteMeal = "DELETE FROM " + TableMeal + " where " + RestId + " = ";
    public static final String DeleteExtraServices = "DELETE FROM " + TableExtraServices + " where " + RestId + " = ";

}
